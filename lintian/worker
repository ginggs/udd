#!/usr/bin/ruby -w

require 'json'
require 'fileutils'

MIRROR='cdn-aws.deb.debian.org'
#MIRROR='10.0.2.2:3142'
DEBUG=true
TIMEOUT=86400 # 1 day
GET_TIMEOUT=1800 # 30 mins

s = STDIN.read
if DEBUG
  File::open('/tmp/lw-input', 'w') do |fd|
    fd.print s
  end
end

d = JSON::parse(s)

def to_dsc_url(source, version)
  if source !~ /^lib/
    p = source[0]
  else
    p = source[0..3]
  end
  version = version.gsub(/^\d+:/, '')
  return "http://#{MIRROR}/debian/pool/main/#{p}/#{source}/#{source}_#{version}.dsc"
end

def to_bin_url(bin)
  return "http://#{MIRROR}/debian/#{bin['filename']}"
end

o = {}
src = d['source']
dir = `mktemp -d /tmp/lw.#{src[0]}.src.XXXXXX`.chomp
Dir::chdir(dir)
begin
  url = to_dsc_url(src[0], src[1])
  Process.setproctitle("lintian-worker #{src[0]}/#{src[1]} source")
  system("timeout #{GET_TIMEOUT} dget --quiet --download-only --allow-unauthenticated #{url} >/dev/null 2>&1") or raise "failed to dget #{url}"
  file = url.split('/')[-1]
  s = `TMPDIR=#{dir} timeout #{TIMEOUT} lintian --allow-root --no-cfg --color never --fail-on none -E -I -L "+=pedantic" -L "+=classification" --show-overrides #{file}`.chomp
  if $?.exitstatus == 124
    raise "Timeout while processing #{file}"
  elsif $?.exitstatus != 0
    raise "Command exited with code #{$?.exitstatus}"
  end
  o['source_output'] = s
rescue
  raise
ensure
  Dir::chdir('/')
  FileUtils::rm_rf(dir)
end

o['binaries_output'] = []
nbin = d['binaries'].length
i = 0
d['binaries'].each do |bin|
  i += 1
  dir = `mktemp -d /tmp/lw.#{src[0]}.bin.XXXXXX`.chomp
  Dir::chdir(dir)
  begin
    url = to_bin_url(bin)
    system("timeout #{GET_TIMEOUT} wget --quiet #{url} >/dev/null 2>&1") or raise "failed to download #{url}"
    file = url.split('/')[-1]
    Process.setproctitle("lintian-worker #{src[0]}/#{src[1]} binary[#{i}/#{nbin}] #{file}")
    s = `TMPDIR=#{dir} timeout #{TIMEOUT} lintian --allow-root --no-cfg --color never --fail-on none -E -I -L "+=pedantic" -L "+=classification" --show-overrides #{file}`.chomp
    if $?.exitstatus == 124
      raise "Timeout while processing #{file}"
    elsif $?.exitstatus != 0
      raise "Command exited with code #{$?.exitstatus}"
    end
    o['binaries_output'] << { 'pkg' => bin, 'output' => s }
  rescue
    raise
  ensure
    Dir::chdir('/')
    FileUtils::rm_rf(dir)
  end
end

puts JSON::dump(o)
exit(0)
