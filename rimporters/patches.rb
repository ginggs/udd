=begin
create table sources_files (
  source text,
  version debversion,
  distribution text,
  release text,
  component text,
  format text,
  hash text,
  size bigint,
  file text
);

CREATE VIEW sources_patches AS
  SELECT source,version,distribution,release,component,format,hash,size,file FROM sources_files
  where format='3.0 (quilt)' and file ~ 'debian\.tar\.([a-zA-Z0-9]+)$'
      and release in (select release from releases where sort >= (select sort from releases where role='oldstable'));


create table patches_status (
  hash text,
  file text,
  status text
);

create table patches (
  hash text,
  file text,
  patch text,
  description text,
  forwarded text,
  last_update timestamp,
  author text,
  origin text,
  forwarded_short text,
  headers text
);

CREATE MATERIALIZED VIEW sources_patches_status AS
SELECT source, version, distribution, release, component, format, status, 
COUNT(DISTINCT patch) as patches,
SUM(CASE WHEN forwarded_short='yes' THEN 1 ELSE 0 END) as forwarded_yes,
SUM(CASE WHEN forwarded_short='no' THEN 1 ELSE 0 END) as forwarded_no,
SUM(CASE WHEN forwarded_short='not-needed' THEN 1 ELSE 0 END) as forwarded_not_needed,
SUM(CASE WHEN forwarded_short='invalid' THEN 1 ELSE 0 END) as forwarded_invalid
FROM sources_patches
LEFT JOIN patches USING (hash, file)
LEFT JOIN patches_status USING (hash, file)
GROUP BY source, version, distribution, release, component, format, status
UNION
SELECT DISTINCT source, version, distribution, release, component, format, 'other format', NULL::bigint, NULL::bigint, NULL::bigint, NULL::bigint, NULL::bigint
FROM sources_files
LEFT JOIN sources_patches USING (source, version, distribution, release, component, format)
WHERE sources_patches.source IS NULL
AND sources_files.release in (select release from releases where sort >= (select sort from releases where role='oldstable'));

SELECT release,
  COUNT(DISTINCT source) as sources,
  SUM(CASE WHEN format='3.0 (quilt)' THEN 1 ELSE 0 END) as format3_0quilt,
  SUM(CASE WHEN format='3.0 (native)' THEN 1 ELSE 0 END) as format3_0native,
  SUM(CASE WHEN format='1.0' THEN 1 ELSE 0 END) as format1_0,
  SUM(CASE WHEN format='3.0 (quilt)' and patches > 0 THEN 1 ELSE 0 END) as p3patches,
  SUM(patches) as patches,
  SUM(forwarded_invalid) AS forwarded_invalid,
  SUM(forwarded_no) AS forwarded_no,
  SUM(forwarded_not_needed) AS forwarded_not_needed,
  SUM(forwarded_yes) AS forwarded_yes
FROM sources_patches_status
WHERE release IN ('buster', 'bullseye', 'bookworm', 'sid')
GROUP BY release

# reimport all patches:
select count(*) from patches_status WHERE status = 'patches' and hash ~ '^a';
DELETE FROM patches_status WHERE status = 'patches' and hash ~ '^a';
DELETE FROM patches WHERE (hash, file) NOT IN (SELECT hash, file FROM patches_status);
REFRESH MATERIALIZED VIEW sources_patches_status;

## TODO highlighting: ruby-rouge
=end

def update_patches
  $db = PG.connect({ :dbname => 'udd', :port => 5452})

  # Refresh sources_files
  $db.exec('BEGIN')
  $db.exec('DELETE FROM sources_files')
  $db.exec <<-EOF
  INSERT INTO sources_files
  WITH files AS (
     SELECT source, version, distribution, release, component, format,
     unnest(string_to_array(checksums_sha256, E'\n')) AS fileline
     FROM sources_uniq
  )
  SELECT source, version, distribution, release, component, format,
    split_part(fileline, ' ', 2) as hash,
    split_part(fileline, ' ', 3)::bigint as size,
    split_part(fileline, ' ', 4) as file
  FROM files WHERE fileline != ''
  EOF
  $db.exec("COMMIT")

  # Import unknown patches
  $db.prepare('insert_patches', 'INSERT INTO patches(hash, file, patch, description, forwarded, forwarded_short, last_update, author, origin, headers, upstream_bug, debian_bug, invalid_reason, forwarded_url) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)')
  $db.prepare('insert_patches_status', 'INSERT INTO patches_status(hash, file, status) VALUES ($1, $2, $3)')

  patches_todo_q = <<-EOF
  SELECT hash, file, component, array_agg(release) as releases
  FROM sources_patches
  WHERE (hash, file) NOT IN (
     SELECT hash, file
     FROM patches_status
  )
  GROUP BY hash, file, component
  EOF
  patches_todo = $db.exec(patches_todo_q).to_a
  # puts "Patches to process: #{patches_todo.length}"
  patches_todo = patches_todo.shuffle
  ret = true
  patches_todo.each do |e|
    begin
      import_patches(e['hash'], e['file'], e['component'], e['releases'])
    rescue
      ret = false
      puts "Failed to import patch: hash=#{e['hash']} file=#{e['file']} component=#{e['component']} releases=#{e['releases']}"
      puts "Exception: #{$!.message}"
    end
  end

  # Cleanup: remove unknown patches from tables
  $db.exec("BEGIN")
  $db.exec("DELETE FROM patches_status WHERE (hash, file) NOT IN (SELECT hash, file FROM sources_patches)")
  $db.exec("DELETE FROM patches WHERE (hash, file) NOT IN (SELECT hash, file FROM patches_status)")
  $db.exec("REFRESH MATERIALIZED VIEW sources_patches_status")
  $db.exec("COMMIT")
  if not ret
    puts "Failed to refresh at least some patches."
    exit(1)
  end
end

def get_patch_path(hash, file, component, releases)
  releases = releases.delete('{}').split(',')
  # If there's a release that isn't a security one, use the normal mirror. else, use the security mirror. Workaround for #1055035
  if releases.any? { |e| e !~ /security/ }
    mirror = "/srv/mirrors/debian/"
  else
    mirror = "/srv/mirrors/debian-security/"
  end
  source = file.split('_', 2)[0]
  d = (source =~ /^lib/) ? source[0..3] : source[0]
  path = "#{mirror}pool/#{component}/#{d}/#{source}/#{file}"
  raise "Unable to find patch file #{file}" if not File::exist?(path)
  return path
end

def extract_patches(path)
  d = `mktemp -d /tmp/patches.XXXXXX`.chomp
  system("tar -x -C #{d} -f #{path}") or raise "tar failed with #{file}"
  patches = []
  if File::exist?("#{d}/debian/patches/series")
    IO::readlines("#{d}/debian/patches/series", :encoding => 'UTF-8').each do |patch|
      patch.encode!('UTF-8', :undef => :replace, :invalid => :replace, :replace => "")
      patch = patch.gsub(/[ \t]+#.*$/, '').strip # space + hash => comment (see quilt(1)
      patch = patch.gsub(/^#.*/, '').strip # line starting with #
      # some patches have options (e.g. -p1) after the patch name.
      # ignore everything after a space
      patch = patch.gsub(/ .*$/, '').strip
      next if patch == ''
      patches << { 'patch' => patch, 'content' => IO::read("#{d}/debian/patches/#{patch}", :encoding => 'UTF-8') }
    end
  end
  FileUtils::rm_rf(d)
  return patches
end

def import_patches(hash, file, component, releases)
  path = get_patch_path(hash, file, component, releases)
  # p [hash, file, releases, path]
  patches = extract_patches(path)
  # parse patches. See https://dep-team.pages.debian.net/deps/dep3/
  patches.each do |pa|
    pa['headers'] = pa['content'].encode("UTF-8", invalid: :replace, replace: "").split(/^---/, 2)[0]
    pa['description'] = ''
    pa['headers'] = '' if pa['headers'].nil?
    pa['headers'].each_line do |l|
      if l =~ /^([-A-Za-z]+): (.*)$/
        h = $1 ; v = $2
        if h =~ /^(Description|Subject)$/i
          pa['description'] = v
        elsif h =~ /^Forwarded$/i
          pa['forwarded'] = v
        elsif h =~ /^(Author|From)$/i
          pa['author'] = v
        elsif h =~ /^(Last-Update|Date)$/i
          pa['last_update'] = Time::parse(v) rescue nil
        elsif h =~ /^Bug$/i
          pa['upstream_bug'] = v
        elsif h =~ /^Bug-Debian$/i
          pa['debian_bug'] = v
        elsif h =~ /^origin$/i
          pa['origin'] = v
        else
          # puts "Unknown field: #{h} (value: #{v}) in file #{file}"
        end
      else
        pa['description'] += l
      end
    end
    pa['forwarded'] = '' if pa['forwarded'].nil?
    fw = pa['forwarded'].strip
    if fw == 'not-needed' or fw =~ /^no[tn][- ]needed/i
      pa['forwarded_short'] = 'not-needed'
    elsif fw =~ /^no(t)?$/i
      pa['forwarded_short'] = 'no'
      pa['invalid_reason'] = 'Explicit Forwarded=no'
    elsif fw =~ /^http(s)?:\/\//
      pa['forwarded_short'] = 'yes'
      pa['forwarded_url'] = fw
    elsif fw =~ /^yes$/i
      if pa['upstream_bug']
        if pa['upstream_bug'] =~ /^http(s)?:\/\//
          if pa['upstream_bug'] =~ /bugs\.debian\.org/
            # common error case: misuse of the Bug: header with the Debian bug
            pa['forwarded_short'] = 'invalid'
            pa['invalid_reason'] = 'Forwarded=yes, Debian bug in Bug: field. Bug-Debian: should be used instead.'
          else
            pa['forwarded_short'] = 'yes'
            pa['forwarded_url'] = pa['upstream_bug']
          end
        else
          pa['forwarded_short'] = 'invalid'
          pa['invalid_reason'] = 'Forwarded=yes, Upstream bug, but not a URL'
        end
      else
        pa['forwarded_short'] = 'invalid'
        pa['invalid_reason'] = 'Forwarded=yes, but no Bug field'
      end
    elsif fw == ''
      if pa['upstream_bug']
        if pa['upstream_bug'] =~ /^http(s)?:\/\//
          if pa['upstream_bug'] =~ /bugs\.debian\.org/
            # common error case: misuse of the Bug: header with the Debian bug
            pa['forwarded_short'] = 'invalid'
            pa['invalid_reason'] = 'No forwarded, Debian bug in Bug: field. Bug-Debian: should be used instead.'
          else
            pa['forwarded_short'] = 'yes'
            pa['forwarded_url'] = pa['upstream_bug']
          end
        else
          pa['forwarded_short'] = 'no'
          pa['invalid_reason'] = 'No Forwarded, Upstream bug, but not a URL'
        end
      else
        pa['forwarded_short'] = 'no'
        pa['invalid_reason'] = 'No Forwarded, No Bug field'
      end
    else
      pa['forwarded_short'] = 'invalid'
      pa['invalid_reason'] = 'Invalid value for Forwarded field'
    end
  end
  $db.exec("BEGIN")
  patches.each do |pa|
    $db.exec_prepared('insert_patches', [ hash, file, pa['patch'], pa['description'], pa['forwarded'], pa['forwarded_short'], pa['last_update'], pa['author'], pa['origin'], pa['headers'], pa['upstream_bug'], pa['debian_bug'], pa['invalid_reason'], pa['forwarded_url'] ])
  end
  $db.exec_prepared('insert_patches_status', [ hash, file, patches.empty? ? 'no-patch' : 'patches'])
  $db.exec("COMMIT")
end
