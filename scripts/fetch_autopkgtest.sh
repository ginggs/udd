#!/bin/sh
TARGETDIR=/srv/udd.debian.org/mirrors/autopkgtest
mkdir -p $TARGETDIR
rm -rf $TARGETDIR/*
cd $TARGETDIR

RELEASES="stable
          testing
          unstable
         "
ARCHS="amd64
       arm64
       ppc64el
      "
for release in $RELEASES ; do
    for arch in $ARCHS ; do
       wget -N https://ci.debian.net/data/status/${release}/${arch}/packages.json -O packages_${release}_${arch}.json
    done
done
