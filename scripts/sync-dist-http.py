#!/usr/bin/python3

# Usage: sync-dist-http.py <distro-url> <target-directory>

# Simple script for mirroring an APT repository over HTTP

# Copyright (C) 2022, Jelmer Vernooĳ <jelmer@debian.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <https://www.gnu.org/licenses/>.

import os
from debian.deb822 import Release
import hashlib
import logging
import time
from urllib.error import HTTPError
from urllib.request import urlopen, Request
import ssl

__version__ = '0.0.1'

DEFAULT_HEADERS = {'User-Agent': f'sync-dist-http/{__version__}'}


HASHES = {
    "MD5Sum": hashlib.md5,
    "SHA1": hashlib.sha1,
    "SHA256": hashlib.sha256,
    "SHA512": hashlib.sha512,
}


class FileInconsistency(Exception):
    """Something about a file was different than expected."""


def verify(name, contents, *, expected_size=None, expected_hash=None):
    if (expected_size is not None
            and len(contents) != int(expected_size)):
        raise FileInconsistency(
            f"Expected file size {expected_size} for {name}, "
            f"got {len(contents)}")
    if expected_hash:
        got_hash = HASHES[expected_hash[0]](contents).hexdigest()
        if got_hash != expected_hash[1]:
            raise FileInconsistency(
                f"Expected {expected_hash[0]} for {name} to be "
                f"{expected_hash[1]}, got {got_hash}")


def download_by_hash(base_url, name, expected_hash):
    req = Request(
        f'{base_url}/{os.path.dirname(name)}'
        f'/by-hash/{expected_hash[0]}/{expected_hash[1]}',
        headers=DEFAULT_HEADERS)
    ctx = ssl.create_default_context(cafile = '/etc/ssl/ca-global/ca-certificates.crt')
    return urlopen(req, context = ctx)


def download_by_name(base_url, name, last_modified=None):
    headers = dict(DEFAULT_HEADERS)
    if last_modified:
        headers["If-Modified-Since"] = time.strftime(
            "%a, %d %b %Y %H:%M:%S GMT", time.gmtime(last_modified)
        )
    req = Request(f'{base_url}/{name}', headers=headers)
    ctx = ssl.create_default_context(cafile = '/etc/ssl/ca-global/ca-certificates.crt')
    return urlopen(req, context = ctx)


def fetch(base_url, target_dir, name, *, expected_size=None,
          expected_hash=None, acquire_by_hash=False):
    """Fetch a file in an apt repository.

    Checks whether the hash & size match those expected.

    Tries to skip downloading any files that are already present
    locally.

    Tries to retrieve using by-hash URL if acquire_by_hash=True,
    but falls back to fetching by filename.
    """
    target_path = os.path.join(target_dir, name)
    try:
        last_modified = os.path.getmtime(target_path)
    except FileNotFoundError:
        last_modified = None

    if expected_hash and expected_size is not None:
        try:
            with open(target_path, 'rb') as f:
                verify(name, f.read(), expected_size=expected_size,
                       expected_hash=expected_hash)
        except (FileNotFoundError, FileInconsistency):
            pass
        else:
            # File is already present with expected hash & size
            return

    try:
        if acquire_by_hash and expected_hash:
            try:
                resp = download_by_hash(base_url, name, expected_hash)
            except HTTPError as e:
                if e.status != 404:
                    raise
                logging.debug(
                    'Did not find by-hash URL %s, '
                    'falling back to regular %s/%s', e.geturl(), base_url,
                    name)
                # Fall back to non-hash URL
                resp = download_by_name(base_url, name, last_modified)
        else:
            resp = download_by_name(base_url, name, last_modified)
    except HTTPError as e:
        if e.status == 304:
            with open(target_path, 'rb') as f:
                contents = f.read()
                verify(name, contents, expected_size=expected_size,
                       expected_hash=expected_hash)
                return contents
        if e.status == 404:
            raise FileNotFoundError(None, None, name) from e
        raise RuntimeError(f'Error {e.status} fetching {e.geturl()}') from e

    with resp:
        os.makedirs(os.path.dirname(target_path), exist_ok=True)
        with open(target_path, 'wb') as f:
            contents = resp.read()
            verify(name, contents, expected_size=expected_size,
                   expected_hash=expected_hash)
            f.write(contents)
            return contents


def main(argv=None):
    import argparse
    import subprocess

    parser = argparse.ArgumentParser()
    parser.add_argument('distro_url', type=str)
    parser.add_argument('target_directory', type=str)
    parser.add_argument(
        '--keyring', type=str, help='Path to PGP keyring')
    parser.add_argument('--debug', action='store_true')
    args = parser.parse_args()

    logging.basicConfig(
        format='%(message)s',
        level=logging.DEBUG if args.debug else logging.INFO)

    todo = []
    contents = fetch(args.distro_url, args.target_directory, 'InRelease')
    release = Release(contents)
    if args.keyring:
        try:
            subprocess.run(
                ['gpgv', f'--keyring={args.keyring}'], input=contents,
                check=True)
        except subprocess.CalledProcessError:
            # gpgv already writes errors to stderr
            return 1
    acquire_by_hash = release.get('Acquire-By-Hash', 'no') == 'yes'
    for hn in ['MD5Sum', 'SHA1', 'SHA256', 'SHA512']:
        for entry in release.get(hn, []):
            todo.append((entry['name'], hn, entry[hn], entry['size']))

    ret = 0
    while todo:
        (n, hn, digest, size) = todo.pop()
        try:
            fetch(args.distro_url,
                  args.target_directory, n, expected_size=size,
                  expected_hash=(hn, digest),
                  acquire_by_hash=acquire_by_hash)
        except FileNotFoundError as e:
            logging.error('Missing file: %s', e.filename)
            ret = 1
    return ret


if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv[1:]))
