#!/usr/bin/perl -w
# Last-Modified: <Mon Aug 18 14:29:47 2008>

use strict;
use warnings;
use utf8;
use Encode qw(encode decode resolve_alias is_utf8 encode_utf8);

use FindBin '$Bin';

# We need our own copy of Debbugs::Status for now
use lib $Bin, qw{/srv/bugs.debian.org/perl /org/udd.debian.org/mirrors/bugs.debian.org/perl};

use DBI;
use DBI qw{:sql_types};
use YAML::Syck;
use Time::Local;

use Debbugs::Bugs qw{get_bugs};
use Debbugs::Status qw{read_bug get_bug_status bug_presence};
use Debbugs::Packages qw{getpkgsrc};
use Debbugs::Config qw{:globals %config};
use Debbugs::User;
use Mail::Address;
#use Debbugs::User qw{read_usertags};
use File::stat;
use Time::localtime;

$YAML::Syck::ImplicitTyping = 1;

#Used for measuring time
our $t;
our $timing = 0;
my %pkgsrc = %{getpkgsrc()};
#our @archs = grep {  !/(^m68k$|^sparc|^hurd)/ } @{$config{default_architectures}};
# hardcoded list for trixie, updated on 2023-11-28
our @archs = qw(amd64 arm64 armel armhf i386 mips64el ppc64el s390x);

# Return the list of usernames
sub get_bugs_users {
	my $topdir = "$gSpoolDir/user";
	my @ret = ();
	# see Debbugs::User::filefromemail for why 0...6
	for(my $i = 0; $i < 7; $i++) {
		my $dir = "$topdir/$i";
		opendir DIR, $dir or die "Can't open dir $dir: $!";
		# Replace all occurences of %dd with the corresponding
		# character represented by dd, where dd is a hexadecimal
		# number
		push @ret, map { s/%(..)/chr(hex($1))/ge; $_ } readdir DIR;
	}
	return @ret;
}

sub parse_time {
	if(shift =~ /(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)/) {
		return ($1, $2, $3, $4, $5, $6);
	}
	return undef;
}


sub get_mtime {
	return ((stat(shift))->mtime);
}

sub get_bugs_modified {
	my $subdir = shift;
	my $top_dir = $gSpoolDir;
	my $result = ();
	my $spool = "$top_dir/$subdir";
	foreach my $subsub (glob "$spool/*") {
		if( -d $subsub ) {
#			print "looking for modified bugs in $subsub\n" if $timing;
			foreach my $log (glob "$subsub/*.log") {
				if ($log =~ m;^(.*)/([^/]*)\.log;) {
					my $path = $1;
					my $id = $2;
					# skip bugs with only log file
					# TODO should this be .status of .summary?
					if (-e "$path/$id.status") {
						$result->{$id} = get_mtime($log);
					}
				}
			}
		}
	}
	return $result;
}

sub without_duplicates {
	my %h = ();
	return (grep { ($h{$_}++ == 0) || 0 } @_);
}

sub get_source {
	my $pkg = shift;

	if ($pkg =~ m/,/) {
		my @pkgs = split(/\s*[, ]\s*/, $pkg);
		my %srcs = ();
		foreach my $p (@pkgs) {
			my $src = get_source($p);
			$srcs{$src} = 1;
		}
		return join(",",sort keys %srcs);

	} else {
		my $srcpkg;
		if ($pkg =~ /^src:(.*)/)
		{
			$srcpkg = $1;
		} else {
			$srcpkg = exists($pkgsrc{$pkg}) ? $pkgsrc{$pkg} : $pkg;
		}
		return $srcpkg;
	}
}

sub run_usertags {
	my ($config, $source, $dbh) = @_;
	my %src_config = %{$config->{$source}};

	my $table = $src_config{'usertags-table'} or die "usertags-table not specified for source $source";
	our $timing;
	our $t;


	$t = time();
	# Free usertags table
	$dbh->do("DELETE FROM $table") or die
		"Couldn't empty $table: $!";
	print "Deleting usertags: ",(time() - $t),"s\n" if $timing;
	$t = time();
	# read and insert user tags
	my @users = get_bugs_users();
	foreach my $user (@users) {
		#read_usertags(\%tags, $user);
		my $u = Debbugs::User->new($user);
		my %tags = %{$u->{tags}};
		$user = $dbh->quote($user);
		foreach my $tag (keys %tags) {
			my $qtag = $dbh->quote($tag);
			map { $dbh->do("INSERT INTO $table (email, tag, id) VALUES ($user, $qtag, $_)") or die $! } @{$tags{$tag}};
		}
	}
	# importing usertags for merged bugs
	$dbh->do("insert into bugs_usertags (email, tag, id) select email, tag, merged_with from bugs_usertags bu, bugs_merged_with bmw where bu.id = bmw.id and (email, tag, merged_with) not in (select email, tag, id from bugs_usertags)") or die $!;
	print "Inserting usertags: ",(time() - $t),"s\n" if $timing;
}

sub parse_email {
	my $email = shift;
	# Fix some invalid emails
	if ($email eq "akira yamada / \x{1b}\$B\$d\$^\$\@\$\"\$-\$i\x{1b}(B <akira\@debian.org>") { $email = "akira yamada <akira\@debian.org>"; }
	elsif ($email eq "kitame\@northeye.org (Takuo KITAME / \x{1b}\$BKLL\\\x{1b}(B \x{1b}\$BBsO:\x{1b}(B)") { $email = "kitame\@northeye.org (Takuo KITAME)"; }
	elsif ($email eq "YOSHIFUJI Hideaki / \x{1b}\$B5HF#1QL\@\x{1b}(B  \x{9}<yoshfuji\@ecei.tohoku.ac.jp>") { $email = "YOSHIFUJI Hideaki <yoshfuji\@ecei.tohoku.ac.jp>"; }
	elsif ($email eq "OZAKI Masanobu / \x{1b}\$BHx:j\@5?-\x{1b}(B <ozaki\@astro.isas.ac.jp>") { $email = "OZAKI Masanobu <ozaki\@astro.isas.ac.jp>"; }
	elsif ($email eq "\x{1b}\$B\$D\$\@\$h\$&\$9\$1\x{1b}(B <yosuke-t\@mbox.kyoto-inet.or.jp>") { $email = "<yosuke-t\@mbox.kyoto-inet.or.jp>"; }
	elsif ($email eq "\x{1b}\$B86ED=SG7\x{1b}(B <nirvana\@fg7.so-net.ne.jp>") { $email = "<nirvana\@fg7.so-net.ne.jp>"; }
	elsif ($email eq "salahuddin pasha \\(salahuddin66\\) <salahuddin.debian\@gmail.com>") { $email = "salahuddin pasha - salahuddin66 <salahuddin.debian\@gmail.com>"; }
	elsif ($email eq "\x{1b}\$B0\$5WDE\x{1b}(B \x{1b}\$BBs\x{1b}(B <Taku.Akutsu\@ma5.seikyou.ne.jp>") { $email = "<Taku.Akutsu\@ma5.seikyou.ne.jp>"; }
	elsif ($email eq "akira yamada / \x{1b}\$B\$d\$^\$\@\$\"\$-\$i\x{1b}(B  <akira\@arika.org>") { $email = "akira yamada <akira\@arika.org>"; }
	elsif ($email eq "akira yamada / \x{1b}\$B\$d\$^\$\@\$\"\$-\$i\x{1b}(B  <akira\@debian.org>") { $email = "akira yamada <akira\@arika.org>"; }
	elsif ($email eq "akira yamada / \x{1b}\$B\$d\$^\$\@\$\"\$-\$i\x{1b}(B <akira\@linux.or.jp>") { $email = "akira yamada <akira\@linux.or.jp>"; }
	elsif ($email eq "Araki Yasuhiro \x{1b}\$B9SLZLw9(\x{1b}(B  \x{9}<yasuhi-a\@is.aist-nara.ac.jp>") { $email = "Araki Yasuhiro <yasuhi-a\@is.aist-nara.ac.jp>"; }
	elsif ($email eq "jiangmin (\x{1b}\$BN68+>-L1\x{1b}(B)  \x{9}<jiangmin\@wiener.siss.kyy.saitama") { $email = "jiangmin <jiangmin\@wiener.siss.kyy.saitama>"; }
	elsif ($email eq "KANDA Mitsuru / \x{1b}\$B?\@ED\x{1b}(B \x{1b}\$B=<\x{1b}(B  <kanda\@nn.iij4u.or.jp>") { $email = "KANDA Mitsuru  <kanda\@nn.iij4u.or.jp>"; }
	elsif ($email eq "Masafumi Okada \x{1b}\$B2,ED\x{1b}(B \x{1b}\$B>;;K\x{1b}(B  <mokada\@md.tsukuba.ac.jp>") { $email = "Masafumi Okada <mokada\@md.tsukuba.ac.jp>"; }
	elsif ($email eq "Masanori.Sekino\@fujixerox.co.jp (\x{1b}\$B4XLn\x{1b}(B\x{1b}\$B2mB'\x{1b}(B)") { $email = "<Masanori.Sekino\@fujixerox.co.jp>"; }
	elsif ($email eq "\x{1b}\$B;3K\\IpHO\x{1b}(B <tkym\@angel.ne.jp>") { $email = "<tkym\@angel.ne.jp>"; }
	elsif ($email eq "\x{1b}\$B86ED=SG7\x{1b}(B <nirvana1\@fg7.so-net.ne.jp>") { $email = "<nirvana1\@fg7.so-net.ne.jp>"; }
	elsif ($email eq "\x{1b}\$B\$d\$^\$\@\$1\$s\$8\x{1b}(B <melito\@prm.club.ne.jp>") { $email = "<melito\@prm.club.ne.jp>"; }
	elsif ($email eq "YAMASHITA Junji (\x{1b}\$B;32<\x{1b}(B \x{1b}\$B=c;J\x{1b}(B)  <ysjj\@unixuser.org>") { $email = "YAMASHITA Junji <ysjj\@unixuser.org>"; }
	my @addr = Mail::Address->parse($email);
	return ($addr[0]->phrase, $addr[0]->address);
}

sub update_bugs {

	my $config = shift;
	my $source = shift;
	my $dbh = shift;
	my $bugs = shift;
	my $limit = shift||1000;

	$t = time();
	my $counter = 0;

	my %src_config = %{$config->{$source}};
	my $unarchived_table = $src_config{table};
	my $archived_table = $src_config{'archived-table'};

	my $location = $src_config{archived} ? 'archive' : 'db_h';
	my $table = $src_config{archived} ? $archived_table : $unarchived_table;
	my $other_table = $src_config{archived} ? $unarchived_table : $archived_table;

	my $insert_bugs_handle = $dbh->prepare("INSERT INTO $table ( ".
		"id, ".
		"package, ".
		"source, ".
		"arrival, ".
		"status, ".
		"severity, ".
		"submitter, ".
		"submitter_name, ".
		"submitter_email, ".
		"owner, ".
		"owner_name, ".
		"owner_email, ".
		"done, ".
		"done_name, ".
		"done_email, ".
		"title, ".
		"forwarded, ".
		"last_modified, ".
		"affects_oldstable, ".
		"affects_stable, ".
		"affects_testing, ".
		"affects_unstable, ".
		"affects_experimental, ".
		"affected_packages, ".
		"affected_sources ".
		") VALUES (".
		"\$1, ".
		"\$2, ".
		"\$3, ".
		"to_timestamp(\$4), ".
		"\$5, ".
		"\$6, ".
		"\$7, ".
		"\$8, ".
		"\$9, ".
		"\$10, ".
		"\$11, ".
		"\$12, ".
		"\$13, ".
		"\$14, ".
		"\$15, ".
		"\$16, ".
		"\$17, ".
		"to_timestamp(\$18), ".
		"\$19, ".
		"\$20, ".
		"\$21, ".
		"\$22, ".
		"\$23, ".
		"\$24, ".
		"\$25".
		")");
	my $insert_bugs_packages_handle = $dbh->prepare("INSERT INTO ${table}_packages (id, package, source) VALUES (\$1, \$2, \$3)");
	my $insert_bugs_found_handle = $dbh->prepare("INSERT INTO ${table}_found_in (id, version) VALUES (\$1, \$2)");
	my $insert_bugs_fixed_handle = $dbh->prepare("INSERT INTO ${table}_fixed_in (id, version) VALUES (\$1, \$2)");
	my $insert_bugs_merged_handle = $dbh->prepare("INSERT INTO ${table}_merged_with (id, merged_with) VALUES (\$1, \$2)");
	my $insert_bugs_tags_handle = $dbh->prepare("INSERT INTO ${table}_tags (id, tag) VALUES (\$1, \$2)");
	my $insert_bugs_blocks_handle = $dbh->prepare("INSERT INTO ${table}_blocks (id, blocked) VALUES (\$1, \$2)");
	my $insert_bugs_blockedby_handle = $dbh->prepare("INSERT INTO ${table}_blockedby (id, blocker) VALUES (\$1, \$2)");
	$insert_bugs_handle->bind_param(4, undef, SQL_INTEGER);
	$insert_bugs_handle->bind_param(18, undef, SQL_INTEGER);

	foreach my $bug_nr (@$bugs) {
		$counter++;

		my $start = time();

		foreach my $prefix ($unarchived_table, $archived_table) {
			foreach my $postfix (qw{_packages _merged_with _found_in _fixed_in _tags _blocks _blockedby _stamps}, '') {
				$dbh->do("DELETE FROM $prefix$postfix where id in ($bug_nr)") or die;
			}
		}

		# invalid bugs
		next if ($bug_nr == 538391);
		next if ($bug_nr == 807319);
		next if ($bug_nr == 941205);
		next if ($bug_nr == 948195);
		next if ($bug_nr == 986382);
		next if ($bug_nr == 986503);
		next if ($bug_nr == 986504);
		next if ($bug_nr == 986505);
		next if ($bug_nr == 986587);
		next if ($bug_nr == 986598);
		next if ($bug_nr == 988376);
		next if ($bug_nr == 992165);
		next if ($bug_nr == 1005503);
		next if ($bug_nr == 1010035);
		next if ($bug_nr == 1013808);
		next if ($bug_nr == 1014746);
		next if ($bug_nr == 1014939);
		next if ($bug_nr == 1021896);
		next if ($bug_nr == 1022017);
		next if ($bug_nr == 1022206);

		# Fetch bug using Debbugs
		# Bugs which were once archived and have been unarchived again will appear in get_bugs(archive => 1).
		# However, those bugs are not to be found in location 'archive', so we detect them, and skip them
		my $bug_ref = read_bug(bug => $bug_nr, location => $location) or (print STDERR "Could not read file for bug $bug_nr; skipping\n" and next);
		# Yeah, great, why does get_bug_status not accept a location?
		my %bug = %{get_bug_status(bug => $bug_nr, status => $bug_ref)};

		# Convert data where necessary
		my @found_versions = @{$bug{found_versions}};
		my @fixed_versions = @{$bug{fixed_versions}};
		my @tags = split / /, $bug{keywords};

		# log_modified and date are not necessarily set. If they are not available, they
		# are assumed to be epoch (i.e. bug #4170)
		map {
		if($bug{$_}) {
			$bug{$_} = int($bug{$_});
		} else {
			$bug{$_} = 0;
		}
		} qw{date log_modified};

		my $srcpkg = get_source($bug{package});
		my $affected_packages = $bug{affects};
		my $affected_sources = get_source($bug{affects});

		# split emails
		my (@addr, $submitter_name, $submitter_email, $owner_name, $owner_email, $done_name, $done_email);
		if ($bug{originator}) {
			($submitter_name, $submitter_email) = parse_email($bug{originator});
		} else {
			$submitter_name = '';
			$submitter_email = '';
		}

		if ($bug{owner}) {
			($owner_name, $owner_email) = parse_email($bug{owner});
		} else {
			$owner_name = '';
			$owner_email = '';
		}

		if ($bug{done}) {
			($done_name, $done_email) = parse_email($bug{done});
		} else {
			$done_name = '';
			$done_email = '';
		}

		#Calculate bug presence in distributions
		my ($present_in_oldstable, $present_in_stable, $present_in_testing, $present_in_unstable, $present_in_experimental);
		if($src_config{archived}) {
			$present_in_oldstable = $present_in_stable = $present_in_testing = $present_in_unstable = $present_in_experimental = 'FALSE';
		} else {
			$present_in_oldstable =
			bug_presence(bug => $bug_nr, status => \%bug,
				dist => 'oldstable',
				arch => \@archs);
			$present_in_stable =
			bug_presence(bug => $bug_nr, status => \%bug,
				dist => 'stable',
				arch => \@archs);
			$present_in_testing =
			bug_presence(bug => $bug_nr, status => \%bug,
				dist => 'testing',
				arch => \@archs);
			$present_in_unstable =
			bug_presence(bug => $bug_nr, status => \%bug,
				dist => 'unstable',
				arch => \@archs);
			$present_in_experimental =
			bug_presence(bug => $bug_nr, status => \%bug,
				dist => 'experimental',
				arch => \@archs);

			if(!defined($present_in_oldstable) or !defined($present_in_stable) or !defined($present_in_unstable) or !defined($present_in_testing) or !defined($present_in_experimental)) {
				print "NUMBER: $bug_nr\n";
			}

			if(defined($present_in_oldstable) and ($present_in_oldstable eq 'absent' or $present_in_oldstable eq 'fixed')) {
				$present_in_oldstable = 'FALSE';
			} else {
				$present_in_oldstable = 'TRUE';
			}
			if(defined($present_in_stable) and ($present_in_stable eq 'absent' or $present_in_stable eq 'fixed')) {
				$present_in_stable = 'FALSE';
			} else {
				$present_in_stable = 'TRUE';
			}
			if(defined($present_in_testing) and ($present_in_testing eq 'absent' or $present_in_testing eq 'fixed')) {
				$present_in_testing = 'FALSE';
			} else {
				$present_in_testing = 'TRUE';
			}
			if(defined($present_in_unstable) and ($present_in_unstable eq 'absent' or $present_in_unstable eq 'fixed')) {
				$present_in_unstable = 'FALSE';
			} else {
				$present_in_unstable = 'TRUE';
			}
			if(defined($present_in_experimental) and ($present_in_experimental eq 'absent' or $present_in_experimental eq 'fixed')) {
				$present_in_experimental = 'FALSE';
			} else {
				$present_in_experimental = 'TRUE';
			}
		}

		# Insert data into bugs table
		$insert_bugs_handle->execute(
			$bug_nr,
			encode_utf8($bug{package}),
			encode_utf8($srcpkg),
			$bug{date},
			$bug{pending},
			$bug{severity},
			encode_utf8($bug{originator}),
			encode_utf8($submitter_name),
			encode_utf8($submitter_email),
			encode_utf8($bug{owner}),
			encode_utf8($owner_name),
			encode_utf8($owner_email),
			encode_utf8($bug{done}),
			encode_utf8($done_name),
			encode_utf8($done_email),
			encode_utf8($bug{subject}),
			encode_utf8($bug{forwarded}),
			$bug{log_modified},
			$present_in_oldstable,
			$present_in_stable,
			$present_in_testing,
			$present_in_unstable,
			$present_in_experimental,
			$affected_packages,
			$affected_sources
		) or die $!;

		my $src;
		foreach my $pkg (keys %{{ map { $_ => 1 } split(/\s*[, ]\s*/, $bug{package})}}) {
			$src = get_source($pkg);
			$insert_bugs_packages_handle->execute($bug_nr, $pkg, $src) or die $!;
		}

		# insert data into bug_fixed_in and bug_found_in tables
		foreach my $version (without_duplicates(@found_versions)) {
			$insert_bugs_found_handle->execute($bug_nr, encode_utf8($version)) or die $!;
		}
		foreach my $version (without_duplicates(@fixed_versions)) {
			$insert_bugs_fixed_handle->execute($bug_nr, encode_utf8($version)) or die $!;
		}
		foreach my $mergee (without_duplicates(split / /, $bug{mergedwith})) {
			$insert_bugs_merged_handle->execute($bug_nr, $mergee) or die $!;
		}
		foreach my $blocked (without_duplicates(split / /, $bug{blocks})) {
			$insert_bugs_blocks_handle->execute($bug_nr, $blocked) or die $!;
		}
		foreach my $blocker (without_duplicates(split / /, $bug{blockedby})) {
			$insert_bugs_blockedby_handle->execute($bug_nr, $blocker) or die $!;
		}
		foreach my $tag (without_duplicates(@tags)) {
			$insert_bugs_tags_handle->execute($bug_nr, encode_utf8($tag)) or die $!;
		}

		my $update_stamp_handle = $dbh->prepare("UPDATE ${table}_stamps SET db_updated = \$1 WHERE id = \$2");
		my $update_res = $update_stamp_handle->execute($start,$bug_nr) or die $!;
		if ($update_res < 1) {
			my $insert_stamp_handle = $dbh->prepare("INSERT INTO ${table}_stamps (id, db_updated) VALUES (\$1, \$2)");
			$insert_stamp_handle->execute($bug_nr,$start) or die $!;
		}


#		if ($timing) { print "$bug_nr $counter/".(scalar @$bugs)."\n"; }
		last if ($counter >= $limit);
	}
	print "Inserting $counter bugs: ",(time() - $t),"s\n" if $timing;
	return $counter;
}

sub run {
	my ($config, $source, $dbh) = @_;

	our $t;
	our $timing;
	my %src_config = %{$config->{$source}};
	my $unarchived_table = $src_config{table};
	my $archived_table = $src_config{'archived-table'};
	my $table = $src_config{archived} ? $archived_table : $unarchived_table;

	my $limit = $src_config{'limit'} || 1000;

	my @modified_bugs;

	my $sth = $dbh->prepare("SELECT id,db_updated FROM ${table}_stamps");
	$sth->execute;
	my $bugs_db_updated = $sth->fetchall_hashref('id');

	if($src_config{archived}) {
		# some bugs (the unarchived ones) are in both list. exclude them.
		my %unarchived;
		foreach my $b (get_bugs()) {
			$unarchived{$b} = 1;
		}
		foreach my $b (get_bugs(archive => 1)) {
			push(@modified_bugs, $b) if not $unarchived{$b};
		}
	} else {
		@modified_bugs = get_bugs();
	}

	# import new bugs
	@modified_bugs = grep { ! defined $bugs_db_updated->{$_} } @modified_bugs;
	my $counter = update_bugs($config,$source,$dbh,\@modified_bugs,$limit);
	$limit -= $counter;

	if ($limit > 0) {
		# we updated less bugs than the limit in the config file
		# update some of the olders bugs
		#my $restrict = "WHERE id IN (624507, 694352, 692948, 692979, 654491, 696552, 694748, 667995, 661018)";
		my $restrict = "";
		$sth = $dbh->prepare("SELECT id FROM ${table}_stamps $restrict ORDER BY db_updated LIMIT $limit");
		$sth->execute;
		my $oldest_bugs = $sth->fetchall_hashref('id');

		my @bug_ids = keys %$oldest_bugs;
		$counter = update_bugs($config,$source,$dbh,\@bug_ids,$limit);
		$limit -= $counter;
	}
}

sub run_modified {
	my ($config, $source, $dbh) = @_;

	our $t;
	our $timing;
	my %src_config = %{$config->{$source}};
	my $unarchived_table = $src_config{table};
	my $archived_table = $src_config{'archived-table'};
	my $table = $src_config{archived} ? $archived_table : $unarchived_table;

	my @modified_bugs;

	print "start looking for modified bugs\n" if $timing;
	my $sth = $dbh->prepare("SELECT id,db_updated FROM ${table}_stamps");
	$sth->execute;
	my $bugs_db_updated = $sth->fetchall_hashref('id');

	my $location = $src_config{archived} ? "archive" : "db-h";
	my $bugs_modified = get_bugs_modified($location);

	foreach my $bugid (keys %$bugs_modified) {
		if (
			# no stamp for this bug: new bug
			(!defined($bugs_db_updated->{$bugid})) ||
			# log file was modified after last db update
			($bugs_modified->{$bugid} > $bugs_db_updated->{$bugid}->{"db_updated"})
		) {
			push @modified_bugs,$bugid;
		}
	}
	my $counter = update_bugs($config,$source,$dbh,\@modified_bugs);
}

sub check_commit {
	my ($config, $source, $dbh) = @_;
	my %src_config = %{$config->{$source}};
	my $table = $src_config{table};

	# Check for broken imports
	if (!$src_config{debug}) {
		my $sthc = $dbh->prepare("select count(*) from bugs where id in (select id from bugs_rt_affects_unstable) and id > 500000");
		$sthc->execute();
		my $rowsc = $sthc->fetchrow_array();
		if ($rowsc < 1000) {
			die("Broken bugs import: not enough bugs affecting unstable\n");
		}
	}

#	if (stat($gSpoolDir."/../versions/indices/binsrc.idx")->mtime > $t) {
#		die("Broken bugs import: binsrc.idx changed during import\n");
#	}
#	if (stat($gSpoolDir."/../versions/indices/srcbin.idx")->mtime > $t) {
#		die("Broken bugs import: srcbin.idx changed during import\n");
#	}
#	if (stat($gSpoolDir."/../versions/indices/versions.idx")->mtime > $t) {
#		die("Broken bugs import: versions.idx changed during import\n");
#	}

	if (defined $table) {
		foreach my $postfix (qw{_packages _merged_with _found_in _fixed_in _tags}, '') {
			my $sth = $dbh->prepare("ANALYZE $table$postfix");
			$sth->execute() or die $!;
		}
	}

	if ($source eq "bugs-usertags") {
		my $sth = $dbh->prepare("ANALYZE ".$src_config{'usertags-table'});
		$sth->execute() or die $!;
	}

	print "Analyzing bugs: ",(time() - $t),"s\n" if $timing;

	$dbh->commit();
	print "Committing bugs: ",(time() - $t),"s\n" if $timing;
}

sub daemon_run {
	my ($config, $source, $dbh) = @_;
	our $timing;
	our $t;

	my $limit = 2000;
	my @bugs;

	# 1) unarchived bugs
	# 1.1) find new bugs
	$t = time();
	my $sth = $dbh->prepare("SELECT id,db_updated FROM bugs_stamps");
	$sth->execute;
	my $bugs_db_updated = $sth->fetchall_hashref('id');
	@bugs = get_bugs();
	@bugs = grep { ! defined $bugs_db_updated->{$_} } @bugs;

	# 1.2) find recently modified bugs
	my $bugs_modified = get_bugs_modified('db-h');
	foreach my $bugid (keys %$bugs_modified) {
		if ((defined($bugs_db_updated->{$bugid})) &&
		       	($bugs_modified->{$bugid} > $bugs_db_updated->{$bugid}->{"db_updated"})) {
			push @bugs,$bugid;
		}
	}
	print "Generating list of ".(scalar @bugs)." new+modified unarchived bugs: ".(time() - $t)."s\n" if $timing;
	my $counter = update_bugs($config,'bugs',$dbh,\@bugs);

	# 1.3) other old bugs
	$t = time();
	my $interval_unarchived = '4 hour';
	$sth = $dbh->prepare("SELECT id FROM bugs_stamps where to_timestamp(db_updated) AT TIME ZONE 'UTC' < current_timestamp - interval '$interval_unarchived' ORDER BY db_updated DESC");
	$sth->execute;
	my $oldest_bugs = $sth->fetchall_hashref('id');
	my @bug_ids = keys %$oldest_bugs;
	print "Generating list of ".(scalar @bug_ids)." old unarchived bugs last updated > $interval_unarchived: ".(time() - $t)."s\n" if $timing;
	$counter = update_bugs($config,'bugs',$dbh,\@bug_ids, $limit);
	my $not_imported = scalar @bug_ids - $counter;
	if ($not_imported > 0) {
		print "Not importing $not_imported unarchived bugs due to limit (=$limit)\n";
	}

	# 2) archived bugs
	# 2.1) new archived bugs
	$t = time();
	$sth = $dbh->prepare("SELECT id,db_updated FROM archived_bugs_stamps");
	$sth->execute;
	$bugs_db_updated = $sth->fetchall_hashref('id');
	my @abugs;

	# some bugs (the unarchived ones) are in both list. exclude them.
	my %unarchived;
	foreach my $b (get_bugs()) {
		$unarchived{$b} = 1;
	}
	foreach my $b (get_bugs(archive => 1)) {
		push(@abugs, $b) if not $unarchived{$b};
	}
	@abugs = grep { ! defined $bugs_db_updated->{$_} } @abugs;
	print "Generating list of ".(scalar @abugs)." new archived bugs: ".(time() - $t)."s\n" if $timing;
	$counter = update_bugs($config,'bugs-archive',$dbh,\@abugs);

	# 2.2) other old archived bugs
	$t = time();
	my $interval_archived = '24 hour';
	$sth = $dbh->prepare("SELECT id FROM archived_bugs_stamps where to_timestamp(db_updated) AT TIME ZONE 'UTC' < current_timestamp - interval '$interval_archived' ORDER BY db_updated DESC");
	$sth->execute;
	$oldest_bugs = $sth->fetchall_hashref('id');
	@bug_ids = keys %$oldest_bugs;
	print "Generating list of ".(scalar @bug_ids)." old archived bugs last updated > $interval_archived: ".(time() - $t)."s\n" if $timing;
	$counter = update_bugs($config,'bugs-archive',$dbh, \@bug_ids, $limit);
	$not_imported = scalar @bug_ids - $counter;
	if ($not_imported > 0) {
		print "Not importing $not_imported archived bugs due to limit (=$limit)\n";
	}

	# 3) Usertags
	run_usertags($config, 'bugs-usertags', $dbh);

	# 4) Finishing up
	$t = time();
	foreach my $postfix (qw{_packages _merged_with _found_in _fixed_in _tags}, '') {
		$sth = $dbh->prepare("ANALYZE bugs$postfix");
		$sth->execute() or die $!;
		$sth = $dbh->prepare("ANALYZE archived_bugs$postfix");
		$sth->execute() or die $!;
	}

	$sth = $dbh->prepare("ANALYZE bugs_usertags");
	$sth->execute() or die $!;

	$dbh->commit();
	print "Finishing up: ".(time() - $t)."s\n" if $timing;
}

sub main {
	if(@ARGV != 3) {
		print STDERR "Usage: $0 <config> <command> <source>\n";
		exit 1;
	}

	our $t = time();
	our $timing;

	my $config = LoadFile($ARGV[0]) or die "Could not load configuration: $!";
	my $command = $ARGV[1];
	my $source = $ARGV[2] || '';

	my $dbname = $config->{general}->{dbname};
	my $dbport;
	if ($config->{general}->{dbport} ne '') {
	  $dbport = ";port=".$config->{general}->{dbport};
	} else {
	  $dbport = "";
	}
	# Connection to DB
	my $dbh = DBI->connect("dbi:Pg:dbname=$dbname".$dbport);
	# We want to commit the transaction as a whole at the end
	$dbh->{AutoCommit} = 0;
	$dbh->{pg_enable_utf8} = 1;
	$dbh->do('SET CONSTRAINTS ALL DEFERRED');

	if($command eq 'run') {
		if ($source eq "bugs-usertags") {
			run_usertags($config, $source, $dbh);
		} elsif ($source eq "bugs-modified") {
			run_modified($config, $source, $dbh);
		} elsif ($source eq "bugs") {
			#run_modified($config, $source, $dbh);
			run($config, $source, $dbh);
		} else {
			run($config, $source, $dbh);
		}
		check_commit($config, $source, $dbh);

	} elsif ($command eq 'daemon-run') {
		daemon_run($config, $source, $dbh);
	} else {
		print STDERR "<command> has to be one of run, drop and setup\n";
		exit(1)
	}

}

main();
