#!/bin/sh

set -x
set -e
PGVER=13
sudo sed -i s/httpredir.debian.org/deb.debian.org/ /etc/apt/sources.list
sudo apt-get update
sudo apt-get install -y apache2 postgresql postgresql-plperl-$PGVER postgresql-$PGVER-debversion ruby-debian ruby-oj rsync python3-yaml python3-psycopg2 ruby-pg ruby-sequel ruby-net-ssh ruby-net-scp
# trust local connections
sudo sed -ri 's/(local\s+all\s+all\s+)peer/\1trust/' /etc/postgresql/$PGVER/main/pg_hba.conf
sudo sed -ri 's/(host\s+all\s+all\s+127.0.0.1\/32\s+)md5/\1trust/' /etc/postgresql/$PGVER/main/pg_hba.conf
sudo sed -ri 's/(host\s+all\s+all\s+::1\/128\s+)md5/\1trust/' /etc/postgresql/$PGVER/main/pg_hba.conf
# use port 5452 (same as the real UDD instance on ullmann)
sudo sed -ri 's/^port = 5432/port = 5452/' /etc/postgresql/$PGVER/main/postgresql.conf
sudo sed -ri 's/^#checkpoint_segments = .*/checkpoint_segments = 256/' /etc/postgresql/$PGVER/main/postgresql.conf
sudo sed -ri 's/^#synchronous_commit = .*/synchronous_commit = off/' /etc/postgresql/$PGVER/main/postgresql.conf
sudo sed -ri 's/^#fsync = .*/fsync = off/' /etc/postgresql/$PGVER/main/postgresql.conf
sudo sed -ri 's/^#shared_buffers = .*/shared_buffers = 1GB/' /etc/postgresql/$PGVER/main/postgresql.conf
# restart postgresql
sudo service postgresql restart
# create and configure UDD database
sudo -u postgres dropdb udd || true
sudo -u postgres dropuser vagrant || true
sudo -u postgres dropuser udd || true
sudo -u postgres createuser -DRS udd
sudo -u postgres createuser -DRS vagrant
sudo -u postgres createdb -T template0 -E SQL_ASCII udd
# create the database, named 'udd', forcing the encoding to SQL_ASCII, since that's the format of the export.
# We base it off 'template0' because 'template1' (the default) might be set to UTF8 which prevents creation
# of new SQL_ASCII databases.
sudo -u postgres psql udd -c 'CREATE EXTENSION debversion'
# Also create a guest user (used by CGIs)
sudo -u postgres dropuser guest || true
sudo -u postgres createuser -lDRS guest
sudo -u postgres psql udd -c 'GRANT usage ON schema public TO PUBLIC;'
sudo -u postgres psql udd -c 'GRANT select ON all tables in schema public TO PUBLIC;'
sudo mkdir -p /srv/udd.debian.org/
sudo ln -sfn /vagrant /srv/udd.debian.org/udd

# Apache setup
sudo ln -sf /vagrant/vagrant/apache.conf /etc/apache2/sites-enabled/000-default.conf
sudo ln -sf /etc/apache2/mods-available/cgi.load /etc/apache2/mods-enabled/
sudo ln -sf /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/
sudo rm -f /etc/apache2/conf-enabled/serve-cgi-bin.conf
#
# Run apache2 as the vagrant user. Yes, eek. But this avoids all permission problems.
sudo sed -i 's/APACHE_RUN_USER=www-data/APACHE_RUN_USER=vagrant/' /etc/apache2/envvars
sudo sed -i 's/APACHE_RUN_GROUP=www-data/APACHE_RUN_GROUP=vagrant/' /etc/apache2/envvars
sudo chown -R vagrant:vagrant /var/log/apache2
sudo chown -R vagrant:vagrant /var/lock/apache2
sudo service apache2 restart

echo "
UDD set up at http://localhost:8080/

The database is empty. Either:
- use a tunnel to the real UDD: vagrant ssh -c /vagrant/vagrant/setup-tunnel.sh
- import (parts of) the real UDD:
    vagrant ssh -c '/vagrant/vagrant/populate-db.sh schema'
    vagrant ssh -c '/vagrant/vagrant/populate-db.sh packages'
    vagrant ssh -c '/vagrant/vagrant/populate-db.sh qa'
    vagrant ssh -c '/vagrant/vagrant/populate-db.sh table udd_logs'
  after import, you might need to refresh materialized views:
     sudo -u postgres psql udd -c 'REFRESH MATERIALIZED VIEW lintian_results_agg'
     sudo -u postgres psql udd -c 'REFRESH MATERIALIZED VIEW lintian'
     sudo -u postgres psql udd -c 'REFRESH MATERIALIZED VIEW sources_patches_status'
"
