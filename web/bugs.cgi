#!/usr/bin/ruby
# encoding: utf-8

$:.unshift('../rlibs')
require 'udd-db'
require 'pp'
require 'cgi'
require 'time'
require 'yaml'
$LOAD_PATH.unshift File.dirname(__FILE__) + '/inc'
require 'dmd-data'
require File.expand_path(File.dirname(__FILE__))+'/inc/page'

#STDERR.reopen(STDOUT)
#puts "Content-type: text/plain\n\n"

UREL=YAML::load(IO::read('ubuntu-releases.yaml'))

tstart = Time::now
DB = Sequel.connect(UDD_GUEST)
DB["SET statement_timeout TO 90000"]

DB.extension :pg_array

rowsr = DB["select * from releases where role != '' order by sort"].all.sym2str
release_roles = {}
releases = []
rowsr.each do |r|
  release_roles[r['role']] = r['release']
  if (r['role'] == 'testing' || /^(old)*stable/.match(r['role']))
    releases << r['release']
  end
end
r_oldstable = release_roles['oldstable']
r_stable = release_roles['stable']
r_testing = release_roles['testing']


RELEASE_RESTRICT = [
  [r_testing, r_testing, 'id in (select id from bugs_rt_affects_testing)'],
  ['sid', 'sid', 'id in (select id from bugs_rt_affects_unstable)'],
  ["#{r_testing}_and_sid", "#{r_testing} and sid", 'id in (select id from bugs_rt_affects_testing) and id in (select id from bugs_rt_affects_unstable)'],
  ["#{r_testing}_or_sid", "#{r_testing} or sid", 'id in (select id from bugs_rt_affects_testing union select id from bugs_rt_affects_unstable)'],
  ["#{r_testing}_not_sid", "#{r_testing}, not sid", 'id in (select id from bugs_rt_affects_testing) and id not in (select id from bugs_rt_affects_unstable)'],
  ["sid_not_#{r_testing}", "sid, not #{r_testing}", 'id in (select id from bugs_rt_affects_unstable) and id not in (select id from bugs_rt_affects_testing)'],
  [r_stable, r_stable, 'id in (select id from bugs_rt_affects_stable)'],
  [r_oldstable, r_oldstable, 'id in (select id from bugs_rt_affects_oldstable)'],
  ['any', 'any', 'id in (select id from bugs where status!=\'done\')'],
  ['na', 'not considered', 'true'],
]

filterlist = [
  ['confirmed', 'tagged confirmed', 'id in (select id from bugs_tags where tag=\'confirmed\')'],
  ['patch', 'tagged patch', 'id in (select id from bugs_tags where tag=\'patch\')'],
  ['pending', 'tagged pending', 'id in (select id from bugs_tags where tag=\'pending\')'],
  ['security', 'tagged security', 'id in (select id from bugs_tags where tag=\'security\')'],
  ['wontfix', 'tagged wontfix', 'id in (select id from bugs_tags where tag=\'wontfix\')'],
  ['moreinfo', 'tagged moreinfo', 'id in (select id from bugs_tags where tag=\'moreinfo\')'],
  ['upstream', 'tagged upstream', 'id in (select id from bugs_tags where tag=\'upstream\')'],
  ['unreproducible', 'tagged unreproducible', 'id in (select id from bugs_tags where tag=\'unreproducible\')'],
  ['help', 'tagged help', 'id in (select id from bugs_tags where tag=\'help\')'],
  ['d-i', 'tagged d-i', 'id in (select id from bugs_tags where tag=\'d-i\')'],
  ['ftbfs', 'tagged ftbfs', 'id in (select id from bugs_tags where tag=\'ftbfs\')'],
  ['forwarded', 'forwarded upstream', 'forwarded != \'\''],
  ['claimed', 'claimed bugs', "id in (select id from bugs_usertags where email='bugsquash@qa.debian.org')"],
  ['deferred', 'fixed in deferred/delayed', "id in (select id from deferred_closes)"],
  ['notmain', 'packages not in main', 'id not in (select id from bugs_packages, sources where bugs_packages.source = sources.source and component=\'main\')'],
]
releases.each do |r|
  filterlist << ["not#{r}", "packages not in #{r}", "id not in (select id from bugs_packages, sources where bugs_packages.source = sources.source and release=\'#{r}\')"]
end
filterlist += [
  ['base', 'packages in base system', 'bugs.source in (select source from sources where priority=\'required\' or priority=\'important\')'],
  ['standard', 'packages in standard installation', 'bugs.source in (select source from sources where priority=\'standard\')'],
  ['orphaned', 'orphaned packages', 'bugs.source in (select source from orphaned_packages where type in (\'ITA\', \'O\'))'],
  ['merged', 'merged bugs', 'id in (select id from bugs_merged_with where id > merged_with)'],
  ['done', 'marked as done', 'status = \'done\''],
]
releases.each do |r|
  filterlist << ["outdated#{r}", "outdated binaries in #{r}", "bugs.source in (select distinct p1.source from packages_summary p1, packages_summary p2 where p1.source = p2.source and p1.release='#{r}' and p2.release='#{r}' and p1.source_version != p2.source_version)"]
end
filterlist += [
  ['outdatedsid', 'outdated binaries in sid', "bugs.source in (select distinct p1.source from packages_summary p1, packages_summary p2 where p1.source = p2.source and p1.release='sid' and p2.release='sid' and p1.source_version != p2.source_version)"],
  ['needmig', "different versions in #{r_testing} and sid", "bugs.source in (select s1.source from sources s1, sources s2 where s1.source = s2.source and s1.release = '#{r_testing}' and s2.release='sid' and s1.version != s2.version)"],
  ['newerubuntu', 'newer in Ubuntu than in sid', "bugs.source in (select s1.source from sources_uniq s1, ubuntu_sources s2 where s1.source = s2.source and s1.release = 'sid' and s2.release='#{UREL['devel']}' and s1.version < s2.version)"],
]
releases.each do |r|
  filterlist +=
  [
    ["rt#{r}-ignore", "RT tag for #{r}: ignore", "id in (select id from bugs_tags where tag='#{r}-ignore')"],
    ["rt#{r}-will-remove", "RT tag for #{r}: will-remove", "id in (select id from bugs_usertags where email='release.debian.org@packages.debian.org' and tag='#{r}-will-remove')"],
    ["rt#{r}-can-defer", "RT tag for #{r}: can-defer", "id in (select id from bugs_usertags where email='release.debian.org@packages.debian.org' and tag='#{r}-can-defer')"],
    ["rt#{r}-is-blocker", "RT tag for #{r}: is-blocker", "id in (select id from bugs_usertags where email='release.debian.org@packages.debian.org' and tag='#{r}-is-blocker')"],
    ["rt#{r}-no-auto-remove", "RT tag for #{r}: no-auto-remove", "id in (select id from bugs_usertags where email='release.debian.org@packages.debian.org' and tag='#{r}-no-auto-remove')"],
    ["rt#{r}-pu", "RT tag for #{r}: pu", "id in (select id from bugs_usertags where email='release.debian.org@packages.debian.org' and tag='pu') and id in (select id from bugs_tags where tag='#{r}')"],
  ]
end
filterlist += [
  ['unblock-hint', 'RT unblock hint', "bugs.source in (select hints.source from hints where type in ('approve','unblock'))"],
  ['mig-blocked', 'RT blocked', "'block' = any(migration_excuses.reason)"],
  ['mig-age', 'RT too young', "policy_info::json#>>'{age,verdict}' != 'PASS'"],
  ['mig-ready', 'RT ready to migrate', "migration_policy_verdict in ('PASS', 'PASS_HINTED')"],
  ['keypackages', 'key packages', 'bugs.id in (select id from bugs_packages bp, key_packages kp where bp.source = kp.source)'],
  ['pseudopackages', 'pseudo packages', 'package in (select package from pseudo_packages)'],
  ['autoremovals', 'packages marked for autoremoval', 'bugs.id in (select id from bugs_packages bp, testing_autoremovals ar where bp.source = ar.source)'],
  ['closedinftpnew', 'closed in packages in new', 'bugs.id in (select id from potential_bug_closures where origin=\'ftpnew\')'],
]

TYPES = [
  ['rc', 'release-critical bugs', 'severity >= \'serious\'', true ],
  ['ipv6', 'release goal: IPv6 support', 'id in (select id from bugs_tags where tag=\'ipv6\')', false ],
  ['lfs', 'release goal: Large File Support', 'id in (select id from bugs_tags where tag=\'lfs\')', false ],
  ['boot', 'release goal: boot performance (init.d dependencies)', 'id in (select id from bugs_usertags where email = \'initscripts-ng-devel@lists.alioth.debian.org\')', false],
  ['oldgnome', 'release goal: remove obsolete GNOME libraries', 'id in (select id from bugs_usertags where email = \'pkg-gnome-maintainers@lists.alioth.debian.org\' and tag=\'oldlibs\')', false],
  ['d-i', 'installer bugs', "bugs.source in (select source from sources where maintainer ~ 'debian-boot@lists.debian.org') OR package like '%-udeb'"],
  ['ruby', 'Ruby bugs', "bugs.source in (select source from sources where maintainer ~ 'ruby' or uploaders ~ 'ruby')\nOR bugs.package in (select source from packages where (package ~ 'ruby' or depends ~ 'ruby') and source != 'subversion')\nOR title ~ 'ruby'"],
  ['systemd', 'pkg-systemd bugs', "bugs.source in (select source from sources where maintainer ~ 'pkg-systemd-maintainers@lists.alioth.debian.org')"],
  ['php', 'PHP bugs', "bugs.source in (select source from packages_summary where package in (select package from debtags where tag = 'implemented-in::php'))"],
  ['l10n', 'Localisation bugs', 'id in (select id from bugs_tags where tag=\'l10n\')', false],
  ['xsf', 'X Strike Force bugs', "bugs.source in (select source from sources where maintainer ~ 'debian-x@lists.debian.org')\n"],
  ['perl', 'Perl team', "bugs.source in (select source from sources where maintainer ~ 'pkg-perl-maintainers@lists.alioth.debian.org')\n"],
  ['java', 'Java team', "bugs.source in (select source from sources where maintainer ~ 'pkg-java-maintainers@lists.alioth.debian.org' or maintainer ~ 'openjdk@lists.launchpad.net')\n"],
  ['games', 'Games team', "bugs.source in (select source from sources where maintainer ~ 'pkg-games-devel@lists.alioth.debian.org')\n"],
  ['multimedia', 'Multimedia team', "bugs.source in (select source from sources where maintainer ~ 'pkg-multimedia-maintainers@lists.alioth.debian.org')"],
  ['otr', 'pkg-otr-team bugs', "bugs.source in (select source from sources where maintainer ~ 'pkg-otr-team@lists.alioth.debian.org' or uploaders ~ 'pkg-otr-team@lists.alioth.debian.org')"],
  ['apparmor', 'AppArmor team', "bugs.source in (select source from sources where maintainer ~ 'pkg-apparmor-team@lists.alioth.debian.org' or uploaders ~ 'pkg-apparmor-team@lists.alioth.debian.org')"],
  ['reproducible', 'Reproducible Builds team', 'id in (select id from bugs_usertags where email = \'reproducible-builds@lists.alioth.debian.org\')', false],
  ['kfreebsd', 'GNU/kFreeBSD bugs', 'id in (select id from bugs_usertags where email = \'debian-bsd@lists.debian.org\' and tag=\'kfreebsd\')', false],
  ['hurd', 'GNU/Hurd bugs', 'id in (select id from bugs_usertags where email = \'debian-hurd@lists.debian.org\' and tag=\'hurd\')', false],
  ['gift', 'bugs tagged newcomer or <a href="https://wiki.debian.org/qa.debian.org/GiftTag">Gift</a>', 'id in (select id from bugs_usertags where email = \'debian-qa@lists.debian.org\' and tag=\'gift\' union select id from bugs_tags where tag=\'newcomer\')', false],
  ['allbugs', 'All bugs', 'true', false],
]

SORTS = [
  ['id', 'bug#'],
  ['source', 'source package'],
  ['package', 'binary package'],
  ['last_modified', 'last modified'],
  ['severity', 'severity'],
  ['popcon', 'popularity contest'],
  ['autormtime', 'autoremoval time'],
  ['lastupload_s', 'last upload in sid'],
]

COLUMNS = [
  ['cpopcon', 'popularity&nbsp;contest'],
  ['chints', 'release&nbsp;team&nbsp;hints'],
  ['cseverity', 'severity'],
  ['ckeypackage', 'key&nbsp;packages'],
  ['ctags', 'tags'],
  ['cclaimed', 'claimed&nbsp;by'],
  ['cdeferred', 'deferred/delayed'],
  ['caffected', 'affected&nbsp;releases'],
  ['cmissingbuilds', 'missing builds in sid'],
  ['clastupload', 'last upload in sid'],
  ['crttags', 'release&nbsp;team&nbsp;tags'],
  ['cautormtime', 'autoremoval&nbsp;time'],
  ['cwhykey', 'reason for being key package'],
  ['cbritney', 'britney status'],
  ['caffected_packages', 'affected&nbsp;packages']
]

# copied from /org/bugs.debian.org/etc/config
tagssingleletter = {
  'patch' => '+',
  'wontfix' => '☹',
  'moreinfo' => 'M',
  'unreproducible' => 'R',
  'security' => 'S',
  'pending' => 'P',
  'fixed'   => 'F',
  'help'    => 'H',
  'fixed-upstream' => 'U',
  'upstream' => 'u',
  # Added tags
  'confirmed' => 'C',
  'l10n' => 'l10n',
  'd-i' => 'd-i',
  'ipv6' => 'ipv6',
  'lfs' => 'lfs',
  'fixed-in-experimental' => 'fie',
  'ftbfs' => '⛺',
}
rowst = DB["select * from bts_tags where tag_type='release'"].all.sym2str
rowst.each do |r|
  tag = r['tag']
  tagssingleletter[tag] = tag[0,3]
end
rowst = DB["select * from bts_tags where tag like '%-ignore'"].all.sym2str
rowst.each do |r|
  tag = r['tag']
  tagssingleletter[tag] = tag[0,3]+'-i'
end
TagsSingleLetter = tagssingleletter


cgi = CGI::new
# releases
if RELEASE_RESTRICT.map { |r| r[0] }.include?(cgi.params['release'][0])
  release = cgi.params['release'][0]
else
  release = r_testing
end
# columns
cols = {}
COLUMNS.map { |r| r[0] }.each do |r|
  if cgi.params[r][0]
    cols[r] = true
  else
    cols[r] = false
  end
end
# sorts
if SORTS.map { |r| r[0] }.include?(cgi.params['sortby'][0])
  sortby = cgi.params['sortby'][0]
else
  sortby = 'id'
end
if ['asc', 'desc'].include?(cgi.params['sorto'][0])
  sorto = cgi.params['sorto'][0]
else
  sorto = 'asc'
end
# hack to enable popcon column if sortby = popcon
cols['cpopcon'] = true if sortby == 'popcon'
cols['cautormtime'] = true if sortby == 'autormtime'
cols['cseverity'] = true if sortby == 'severity'
cols['clastupload'] = true if sortby == 'lastupload_s'
# filters
j_mig = false
filters = {}
filterlist.map { |r| r[0] }.each do |e|
  if ['', 'only', 'ign'].include?(cgi.params[e][0])
    filters[e] = cgi.params[e][0]
  else
    filters[e] = (e == 'merged' ? 'ign' : '')
  end
  if (e =~ /^mig/) && (filters[e] != '')
    # join migration_excuses table
    j_mig = true
  end
end
# filter: newer than X days
if ['', 'only', 'ign'].include?(cgi.params['fnewer'][0])
  fnewer = cgi.params['fnewer'][0]
else
  fnewer = ''
end
if cgi.params['fnewerval'][0] =~ /^[0-9]+$/
  fnewerval = cgi.params['fnewerval'][0].to_i
else
  fnewerval = 7
end
if ['', 'only', 'ign'].include?(cgi.params['flastmod'][0])
  flastmod = cgi.params['flastmod'][0]
else
  flastmod = ''
end
if cgi.params['flastmodval'][0] =~ /^[0-9]+$/
  flastmodval = cgi.params['flastmodval'][0].to_i
else
  flastmodval = 7
end
if ['', 'only', 'ign'].include?(cgi.params['fblocking'][0])
  fblocking = cgi.params['fblocking'][0]
else
  fblocking = ''
end
if cgi.params['fblockingval'][0] =~ /^[0-9]+$/
  fblockingval = cgi.params['fblockingval'][0].to_i
else
  fblockingval = ''
end
if ['', 'only', 'ign'].include?(cgi.params['fusertag'][0])
  fusertag = cgi.params['fusertag'][0]
else
  fusertag = ''
end
# TODO what characters can be used in a usertag?
if cgi.params['fusertaguser'][0] =~ /^[a-z0-9.+-]+@[a-z0-9.-]+$/i
  fusertaguser = cgi.params['fusertaguser'][0]
else
  fusertaguser = ''
end
if cgi.params['fusertagtag'][0] =~ /^[a-z0-9_.-]+$/i
  fusertagtag = cgi.params['fusertagtag'][0]
else
  fusertagtag = ''
end
# types
types = {}
TYPES.each do |t|
  if cgi.params == {}
    types[t[0]] = t[3]
  else
    if cgi.params[t[0]][0] == '1'
      types[t[0]] = true
    else
      types[t[0]] = false
    end
  end
end

dmd = false
if cgi.params['dmd'][0] == '1'
  dmd = true
end
dmdparams = UDDData.parse_cgi_params(cgi.params)

def genaffected(r)
  s = ""
  s += "<abbr title='affects stable'>S</abbr>" if r['affects_stable']
  s += "<abbr title='affects testing'>T</abbr>" if r['affects_testing']
  s += "<abbr title='affects unstable'>U</abbr>" if r['affects_unstable']
  s += "<abbr title='affects experimental'>E</abbr>" if r['affects_experimental']
  s += "<abbr title='in experimental, but not affected'><b>ē</b></abbr>" if $sources_in_experimental.include?(r['source']) and not r['affects_experimental']
  return "" if s == ""
  return "("+s+")"
end

if cgi.params != {}
  # Generate and execute query
  q = "select id, bugs.package, bugs.source, severity, title, last_modified, status, affects_stable, affects_testing, affects_unstable, affects_experimental "
  if cols['cpopcon']
    q += ", coalesce(popcon_src.insts, 0) as popcon"
  end
  if cols['cautormtime']
    # used for display
    q += ", coalesce(testing_autoremovals.removal_time, 0) as autormdate "
    # used for sort
    #  bugs in packages without autoremoval time will be sorted after other bugs
    #  assumes autoremoval_time is less than 1 year from now
    q += ", coalesce(testing_autoremovals.removal_time, "+(tstart.to_i + 365*24*3600).to_s+") as autormtime "
  end
  if cols['cwhykey'] || cols['ckeypackage']
    q += ", coalesce(key_packages.reason, '') as whykey"
  end
  if cols['cbritney'] || j_mig
    q += ", coalesce(migration_excuses.migration_policy_verdict, '') as britney"
    q += ", migration_excuses.reason as excuse_reason"
    q += ", (migration_excuses.policy_info::json#>>'{age,current-age}')::int as currentage"
    q += ", (migration_excuses.policy_info::json#>>'{age,age-requirement}')::int as agerequirement"
    q += ", (array(select json_array_elements_text(migration_excuses.policy_info::json#>'{rc-bugs,unique-source-bugs}'))) as newbugs"
    q += ", migration_excuses.policy_info "
  end
  if cols['cmissingbuilds']
    q += ", coalesce(mb, '-') as missingbuilds"
  end

  if cols['clastupload']
    q += ", to_char(lastupload_s,'YYYY-MM-DD') as lastupload"
  end
  if cols['caffected_packages']
    q += ", affected_packages"
  end

  q += "\nfrom bugs "

  if cols['cpopcon']
    q += " left join popcon_src on (bugs.source = popcon_src.source) "
  end
  if cols['cautormtime']
    q += " left join testing_autoremovals on (bugs.source = testing_autoremovals.source) "
  end
  if cols['cwhykey'] || cols['ckeypackage']
    q += " left join key_packages on (bugs.source = key_packages.source) "
  end
  if cols['cbritney'] || j_mig
    q += " left join migration_excuses on (bugs.source = migration_excuses.item_name) "
  end
  if cols['cmissingbuilds']
    q += " left join (
        select string_agg(distinct(wannabuild.architecture),', ' order by wannabuild.architecture) mb, source
        from wannabuild
        where distribution='sid'
        and not vancouvered
        and notes='out-of-date'
        and state not in ('Installed')
        group by source
        ) as wb on (bugs.source = wb.source) "
  end
  if cols['clastupload']
    q += " left join (
        select max(date) lastupload_s, s1.source
        from sources s1, upload_history uh
        where s1.source = uh.source
        and s1.version = uh.version
        and s1.release='sid'
        group by s1.source
        ) as lu on (bugs.source = lu.source) "
  end

  q += "\n"

  q += "where #{RELEASE_RESTRICT.select { |r| r[0] == release }[0][2]} \n"
  filterlist.each do |f|
    if filters[f[0]] == 'only'
      q += "and #{f[2]} \n"
    elsif filters[f[0]] == 'ign'
      q += "and not (#{f[2]}) \n"
    end
  end
  if fnewer == 'only'
    q += "and (current_timestamp - interval '#{fnewerval} days' <= arrival) \n"
  elsif fnewer == 'ign'
    q += "and (current_timestamp - interval '#{fnewerval} days' > arrival) \n"
  end
  if flastmod == 'only'
    q += "and (current_timestamp - interval '#{flastmodval} days' <= last_modified) \n"
  elsif flastmod == 'ign'
    q += "and (current_timestamp - interval '#{flastmodval} days' > last_modified) \n"
  end
  if fblocking == 'only'
    q += "and (id in (select id from bugs_blocks where blocked = #{fblockingval})) \n"
  elsif fblocking == 'ign'
    q += "and (id not in (select id from bugs_blocks where blocked = #{fblockingval})) \n"
  end

  usertagq = " id in (select id from bugs_usertags where email='#{fusertaguser}'"
  if not fusertagtag.empty?
    usertagq += " and tag = '#{fusertagtag}'"
  end
  usertagq += ") "
  if fusertag == 'only'
    q += "and (#{usertagq}) \n"
  elsif fusertag == 'ign'
    q += "and not (#{usertagq}) \n"
  end

  if (not dmdparams['emails'].empty?) or dmdparams['packages'] != ''
    uddd = UDDData.new(dmdparams['emails'],
                       dmdparams['packages'],
                       dmdparams['bin2src'] == 'on',
                       dmdparams['ignpackages'],
                       dmdparams['ignbin2src'] == 'on')
    mysources = uddd.sources.keys.map { |e| "'#{e}'" }.join(', ')
    if not mysources.empty?
        qdmd = ["bugs.source in (#{mysources})"]
    else
        qdmd = []
        error = "No bug found matching your criteria"
        q += "AND FALSE\n"
    end
  else
    qdmd = []
  end
  q2 = (TYPES.select { |t| types[t[0]] }.map { |t| t[2] } + qdmd).join("\n OR ")
  if q2 != ""
    q += "AND (#{q2})\n"
  else
    error = "Must select at least one bug type!"
    q += "AND FALSE\n"
  end
  q += "order by #{sortby} #{sorto}"

  load = IO::read('/proc/loadavg').split[1].to_f
  if load > 20
    puts "Content-type: text/html\n\n"
    puts "<p><b>Current system load (#{load}) is too high. Please retry later!</b></p>"
    puts "<pre>#{q}</pre>"
    exit(0)
  end

  begin
    rows = DB[q].all.sym2str
  rescue RuntimeError => e
    puts "Content-type: text/html\n\n"
    puts "<p>The query generated an error, please report it to lucas@debian.org: #{e.message}</p>"
    puts "<pre>#{q}</pre>"
    exit(0)
  end

  bugs = []
  rows.each do |r|
    bugs.push(r.to_h)
  end

  if rows.length > 0
    if cols['chints']
      # this used to be 'relevant_hints' instead of hints (which checks the
      # version in unstable) - changed because package info sync is down
      # 2013-03-24 ivodd
      rowsh = DB["select distinct source, type, argument, version, file, comment from hints order by type"].all.sym2str
      hints = {}
      rowsh.each do |r|
        hints[r['source']] ||= []
        hints[r['source']] << r.to_h
      end
      rowsh = DB["select distinct bugs_usertags.id as id, bugs_usertags.tag as tag, bugs.title as title from bugs_usertags, bugs where bugs.id = bugs_usertags.id and bugs_usertags.email in ('release.debian.org@packages.debian.org','ftp.debian.org@packages.debian.org') and bugs_usertags.tag in ('unblock','rm','remove') and bugs.status = 'pending'"].all.sym2str
      unblockreq = {}
      unblockreqtype = {}
      ids = []
      rowsh.each do |r|
        src = (/[^-a-zA-Z0-9.]([-a-zA-Z0-9.]+)\//.match(r['title']) || [] ) [1] || "";
        if src == ""
          if r['title'].split(" ")[1]
            if r['title'].split(" ")[1].split('/')[0]
              src = r['title'].split(" ")[1].split('/')[0]
            end
          end
        end
        unblockreq[src] ||= []
        unblockreq[src] << r['id']
        unblockreqtype[r['id']] = r['tag']
        ids << r['id']
      end
      ids = ids.join(',')
      rowst = DB["select id, tag from bugs_tags where id in (#{ids})"].all.sym2str
      unblockreqtags = {}
      rowst.each do |r|
        unblockreqtags[r['id']] ||= []
        unblockreqtags[r['id']] << r['tag']
      end

      bugs.each do |r|
        r['rt-hints'] = hints[r['source']] if hints[r['source']]
        if unblockreq[r['source']]
          r['unblock-requests'] = unblockreq[r['source']].map { |e| { "type" => unblockreqtype[e], "id" => e, "tags" => unblockreqtags[e] } }
        end
      end
    end

    if cols['ctags']
      ids = rows.map { |r| r['id'] }.join(',')
      rowst = DB["select id, tag from bugs_tags where id in (#{ids})"].all.sym2str
      tags = {}
      rowst.each do |r|
        tags[r['id']] ||= []
        tags[r['id']] << r['tag']
      end

      bugs.each do |r|
        r['tags'] = tags[r['id']] if tags[r['id']]
      end
    end

    if cols['cclaimed']
      ids = rows.map { |r| r['id'] }.join(',')
      rowst = DB["select distinct id, tag from bugs_usertags where email='bugsquash@qa.debian.org' and id in (#{ids})"].all.sym2str
      claimedbugs = {}
      rowst.each do |r|
        claimedbugs[r['id']] ||= []
        claimedbugs[r['id']] << r['tag']
      end
      bugs.each do |r|
        r['claimed'] = claimedbugs[r['id']] if claimedbugs[r['id']]
      end
    end

    if cols['crttags']
      ids = rows.map { |r| r['id'] }.join(',')
      rowst = DB["select distinct id, tag from bugs_usertags where email='release.debian.org@packages.debian.org' and id in (#{ids})"].all.sym2str
      rttags = {}
      rowst.each do |r|
        rttags[r['id']] ||= []
        rttags[r['id']] << r['tag']
      end
      bugs.each do |r|
        r['rttags'] = rttags[r['id']] if rttags[r['id']]
      end
    end

    if cols['cdeferred']
      ids = rows.map { |r| r['id'] }.join(',')
      rowsd = DB["select id, deferred.source, deferred.version, extract (day from delay_remaining) as du from deferred, deferred_closes where deferred.source = deferred_closes.source and deferred.version = deferred_closes.version and deferred_closes.id in (#{ids})"].all.sym2str
      deferredbugs = {}
      rowsd.each do |r|
        d = r['du'].to_i
        deferredbugs[r['id']] = { "version" => r['version'], "days" => d }
      end
      bugs.each do |r|
        if deferredbugs[r['id']]
          t = deferredbugs[r['id']]
          r['deferred'] = t
          r['deferred_text'] = "#{t['version']} (#{t['days']} day#{t['days']==1?'':'s'})"
        end
      end
    end

    if cols['caffected']
      sources = rows.map { |r| r['source'] }.map { |e| "'#{e}'" }.join(',')
      rowsd = DB["select source from sources where source in (#{sources}) and distribution='debian' and release='experimental'"].all.sym2str
      $sources_in_experimental = rowsd.map { |r| r['source'] }
      bugs.each do |r|
        r['affected_html'] = genaffected(r).force_encoding("ASCII-8BIT")
      end
    end

    bugs.each do |r|
      r['last_modified_full'] = r['last_modified'].to_s
      r['last_modified'] = r['last_modified'].to_date.to_s
      if !r['autormdate'] || r['autormdate'] == 0
        r['autormdate'] = ''
      else
        r['autormdate'] = Time::strptime(r['autormdate'].to_s,"%s").to_date.to_s
      end
    end

    if cols['cmissingbuilds']
      sources = rows.map { |r| r['source'] }.map { |e| "'#{e}'" }.join(',')
      rowsd = DB["select source from sources where source in (#{sources}) and distribution='debian' and release='experimental'"].all.sym2str
      $sources_in_experimental = rowsd.map { |r| r['source'] }
      bugs.each do |r|
        r['affected_html'] = genaffected(r).force_encoding("ASCII-8BIT")
      end
    end

    if cols['cbritney']
      bugs.each do |r|
        if (r['britney'] == 'PASS')
            britneyinfo = r['britney']
        else
            britneyinfo = ""
        end
        r_seen = {}
        if r['excuse_reason'].respond_to?('each')
            r['excuse_reason'].each do |reason|
                britneyinfo += " #{reason}"
                r_seen[reason] = 1
            end
        end
        if r['policy_info']
          pi=YAML::load(r['policy_info'])
          pi.each do |policy, info|
            if (info['verdict'] != 'PASS')
              next if r_seen[policy]
              # handled below
              next if policy == 'rc-bugs'
              britneyinfo += " #{policy}"
            end
          end
        end
        currentage = r['currentage']
        agerequirement = r['agerequirement']
        if ((agerequirement||0 > 0) && (currentage < agerequirement))
            britneyinfo += " #{r['currentage']}/#{r['agerequirement']}"
        end
        if r['newbugs'].respond_to?('each')
            r['newbugs'].each do |b|
                britneyinfo += " <a href=\"https://bugs.debian.org/#{b}\">#{b}</a>"
            end
        end
        r['britney_html'] = britneyinfo
      end
    end

  end

  r2 = DB["select max(start_time) from timestamps where source = 'bugs' and command = 'run'"].all.sym2str
  timegen = sprintf "%.3f", Time::now - tstart

  feeditems = []
  bugs.each do |b|
    feeditems.push({:link  => "https://bugs.debian.org/%s" % b['id'],
                    :title => "%s: %s" % [b['package'], b['title']]})
  end
end

format = cgi.params['format'][0]
page = Page.new(bugs, format, 'templates/bugs.erb',
                { :bugsurl => 'https://udd.debian.org/bugs/',
                  :release => release,
                  :RELEASE_RESTRICT => RELEASE_RESTRICT,
                  :filterlist => filterlist,
                  :filters => filters,
                  :TYPES => TYPES,
                  :types => types,
                  :fnewer => fnewer,
                  :fnewerval => fnewerval,
                  :flastmod => flastmod,
                  :flastmodval => flastmodval,
                  :fblocking => fblocking,
                  :fblockingval => fblockingval,
                  :fusertag => fusertag,
                  :fusertaguser => fusertaguser,
                  :fusertagtag => fusertagtag,
                  :sortby => sortby,
                  :sorto => sorto,
                  :cols => cols,
                  :bugs => bugs,
                  :r2 => r2,
                  :q => q,
                  :error => error,
                  :timegen => timegen,
                  :r_testing => r_testing,
                  :params => dmdparams,
                  :dmd => dmd},
                  'Bugs search', feeditems)
page.render()
