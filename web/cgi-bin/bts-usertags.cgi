#!/usr/bin/ruby

$:.unshift('../../rlibs')
require 'udd-db'
require 'pp'
require 'cgi'
require 'json'

#puts "Content-type: text/plain\n\n"; STDERR.reopen(STDOUT)

DB = Sequel.connect(UDD_GUEST)
cgi = CGI.new

format = (cgi['format'] == 'json' ? :json : :html)
user = cgi['user']
user = nil if user.empty?
user = CGI.escape_html(user) if not user.nil?

tag = cgi['tag']
tag = nil if tag.empty?
tag = CGI.escape_html(tag) if not tag.nil?

bug = cgi['bug']
bug = nil if bug.empty?
bug = CGI.escape_html(bug) if not bug.nil?

browse_users = (cgi['browse'] == 'users')

def html_header
  puts "Content-type: text/html\n\n"
  puts <<-EOF
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <style type="text/css">
    label {
      display: inline-block;
      width: 5ex;
    }
    table {
      border-collapse: collapse;
    }
    table, th, td {
      border: 1px solid gray;
      padding: 3px;
    }
    tr.done td {
      text-decoration: line-through;
    }
  </style>
  <title>Debian BTS Usertag Browser</title>
</head>
<body>
  EOF
end

def html_footer
  puts <<-EOF
<h2>Search</h2>
<form action="?" accept-charset="UTF-8">
  <label for="user">User:</label>
  <input type="email" name="user" id="user"><br />
  <label for="tag">Tag:</label>
  <input type="text" name="tag" id="tag"><br />
  <input type="submit" formnovalidate value="Search">
</form>
<h2>By Bug</h2>
<form action="?" accept-charset="UTF-8">
  <label for="bug">Bug:</label>
  <input type="number" name="bug" id="bug" placeholder="Bug #"><br />
  <input type="submit" value="Search">
</form>
<h2>Browse</h2>
<ul>
  <li><a href="?browse=users">By user</a></li>
</ul>
<script type="text/javascript" src="../static/sorttable.js"></script>
</body>
</html>
  EOF
end

if browse_users
  rows = DB["SELECT email, COUNT(*) AS count FROM bugs_usertags INNER JOIN bugs USING (id) GROUP BY email ORDER BY email"].all.sym2str
  if format == :json
    puts "Content-type: application/json\n\n"
    puts JSON.pretty_generate(rows)
  else
    puts html_header
    puts "<h1>User list</h1>"
    puts '<table class="sortable">'
    puts '<thead><tr><td>User</td><td>Bugs</td></tr></thead>'
    rows.each do |r|
      puts "<tr><td><a href=\"?user=#{CGI.escape_html(r['email'])}\">#{CGI.escape_html(r['email'])}</a></td><td>#{r['count']}</td></tr>"
    end
    puts "</table>"
    puts html_footer
  end

elsif user and not tag
  rows = DB["SELECT tag, COUNT(*) AS count FROM bugs_usertags INNER JOIN bugs USING (id) WHERE email = ? GROUP BY tag ORDER BY tag", user].all.sym2str
  if format == :json
    puts "Content-type: application/json\n\n"
    puts JSON.pretty_generate(rows)
  else
    puts html_header
    puts "<h1>User #{CGI.escape_html(user)}</h1>"
    puts '<table class="sortable">'
    puts '<thead><tr><td>Tag</td><td>Bugs</td></tr></thead>'
    rows.each do |r|
      puts "<tr><td><a href=\"?user=#{CGI.escape_html(user)}&tag=#{CGI.escape_html(r['tag'])}\">#{CGI.escape_html(r['tag'])}</a></td><td>#{r['count']}</td></tr>"
    end
    puts "</table>"
    puts html_footer
  end

elsif user and tag
  rows = DB["SELECT id, package, source, title, done != '' AS done FROM bugs INNER JOIN bugs_usertags USING (id) WHERE bugs_usertags.email = ? AND bugs_usertags.tag = ? ORDER BY id", user, tag].all.sym2str
  if format == :json
    puts "Content-type: application/json\n\n"
    puts JSON.pretty_generate(rows)
  else
    puts html_header
    puts "<h1>Tagged #{CGI.escape_html(tag)} by #{CGI.escape_html(user)}</h1>"
    puts '<table class="sortable">'
    puts '<thead><tr><td>State</td><td>Bug</td><td>Source</td><td>Package</td><td>Title</td><td>Other tags</td></tr></thead>'
    rows.each do |r|
      state = r['done'] ? 'done' : 'open'
      puts "<tr><td>#{state}</td>"
      puts "<td><a href=\"https://bugs.debian.org/#{r['id']}\">##{r['id']}</td>"
      puts "<td><a href=\"https://tracker.debian.org/#{CGI.escape_html(r['source'])}\">#{CGI.escape_html(r['source'])}</td>"
      puts "<td><a href=\"https://tracker.debian.org/#{CGI.escape_html(r['source'])}\">#{CGI.escape_html(r['package'])}</td>"
      puts "<td>#{CGI.escape_html(r['title'])}</td>"
      puts "<td><a href=\"?bug=#{r['id']}\">list usertags</a></td></tr>"
    end
    puts "</table>"
    puts html_footer
  end

elsif bug
  rows = DB["SELECT email, tag FROM bugs_usertags WHERE id = ? ORDER BY email, tag", bug].all.sym2str
  if format == :json
    puts "Content-type: application/json\n\n"
    puts JSON.pretty_generate(rows)
  else
    puts html_header
    puts "<h1>Bug ##{CGI.escape_html(bug)}</h1>"
    puts '<table class="sortable">'
    puts '<thead><tr><td>User</td><td>Tag</td></tr></thead>'
    rows.each do |r|
      puts "<tr>"
      puts "<td><a href=\"?user=#{CGI.escape_html(r['email'])}\">#{CGI.escape_html(r['email'])}</a></td>"
      puts "<td><a href=\"?user=#{CGI.escape_html(r['email'])}&tag=#{CGI.escape_html(r['tag'])}\">#{CGI.escape_html(r['tag'])}</a></td>"
      puts "<tr>"
    end
    puts "</table>"
    puts html_footer
  end
else
  puts html_header
  puts html_footer
end
