#!/usr/bin/python3
# -*- coding: utf-8; mode: python; tab-width: 4; -*-

"""
List key packages(?) in YAML format.
"""

import sys
import os
sys.path.insert(0, os.path.abspath('../../pylibs/'))
from cgi_helpers import *
from psycopg2 import connect
from psycopg2.extras import DictCursor
import yaml


DATABASE = 'service=udd'
QUERY = """\
  SELECT *
    FROM key_packages
ORDER BY source
"""

print_contenttype_header('text/plain')

conn = connect(DATABASE)
cur = conn.cursor(cursor_factory=DictCursor)
cur.execute(QUERY)
rows = cur.fetchall()
cur.close()
conn.close()

data = []

for row in rows:
    entry = {}
    entry['source'] = row['source']
    entry['reason'] = row['reason']
    data.append(entry)

print(yaml.dump(data, default_flow_style=False))
