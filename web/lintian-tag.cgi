#!/usr/bin/ruby

FETCH_LIMIT = 5000

$:.unshift('../rlibs')
require 'udd-db'
require 'cgi'
require 'pp'

def esc(s)
  return "" if s.nil?
  CGI.escape_html(s)
end


#puts "Content-type: text/plain\n\n"; STDERR.reopen(STDOUT)

puts "Content-type: text/html; charset=utf-8\n\n"

now = Time::now
cgi = CGI::new

if not cgi['tag'].empty?
  tag = esc(cgi['tag'])
else
  tag = nil
end

affected = (cgi['affected'] == 'yes')

if tag.nil?
  # list all tags
  q = <<-EOF
select distinct tag_type, tag
FROM lintian_tags_descriptions
ORDER BY tag ASC
  EOF
  DB = Sequel.connect(UDD_GUEST)
  res = DB[q].map { |r| r.to_h }

  puts <<-EOF
<!DOCTYPE html><html lang="en"><head>
<link href="/css/debian.css" rel="stylesheet" type="text/css">
<link href="/css/udd.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.1.13.6.css"/>
<link rel="canonical" href="/lintian-tags/"/>
<title>Lintian tags information</title>
<meta name="description" content="Debian Lintian tags list"/>
</head>
<body>
<div id="content">
<h1>Known Lintian tags</h1>

<ul>
  EOF
  res.each do |r|
    puts "<li><a href=\"/lintian-tag/#{CGI.escape(r[:tag])}\">#{esc(r[:tag])} (#{esc(r[:tag_type])})</a></li>"
  end
  puts <<-EOF
</ul>
</div>
</body>
</html>
  EOF
  exit(0)
end

if tag !~ /^[a-z0-9_.+\/-]+$/i
  puts "Invalid characters in tag name"
  exit(0)
end

DB = Sequel.connect(UDD_GUEST)

if affected
  q = <<-EOF
select source, version, package, package_version, architectures::text[] architectures, tag_type, tag, information, lintian_version, count
FROM lintian_results_agg
WHERE tag = '#{tag}'
ORDER BY 1,2,3,4,6,7
LIMIT #{FETCH_LIMIT}
  EOF
  res = DB[q].map { |r| r.to_h }
  res.each do |e|
    # convert array to string (that's something that is only needed in the dev env, strangely)
    if defined?(Sequel::Postgres::PGArray) and e[:architectures].kind_of?(Sequel::Postgres::PGArray)
      e[:architectures] = e[:architectures].join(',')
    end
    e[:architectures] = e[:architectures].gsub('NULL', 'source').gsub(/^\{(.*)\}$/, '\1').split(',').uniq.sort
  end
end

q = "select tag_type, description from lintian_tags_descriptions where tag='#{tag}'"
res2 = DB[q].first
if res2.nil?
  desc = "TAG NOT KNOWN"
  type = "UNKNOWN"
else
  desc = res2[:description]
  type = res2[:tag_type]
end

puts <<-EOF
<!DOCTYPE html><html lang="en"><head>
<link href="/css/debian.css" rel="stylesheet" type="text/css">
<link href="/css/udd.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.1.13.6.css"/>
<link rel="canonical" href="/lintian-tag/#{esc(tag)}#{affected ? '?affected=yes' : ''}"/>
<title>Lintian tag: #{esc(tag)}</title>
<meta name="description" content="Debian Lintian tag information for tag #{esc(tag)}"/>
</head>
<body>
<div id="content">
<h1>Lintian tag: #{esc(tag)}</h1>
<h2>Type: #{esc(type)}</h2>
<h2>Description (from <code>lintian-explain-tags</code>)</h2>
<pre>#{esc(desc)}</pre>
EOF
if not affected
  puts "<p><a href=\"/lintian-tag/#{esc(tag)}?affected=yes\"><b>Show affected packages</b></a></p>"
else
  puts <<-EOF
<h2>Affected packages</h2>
<table class="buglist table table-bordered table-hover display compact" id="lintian">
<thead>
<tr>
    <th>source</th>
    <th>version</th>
    <th>binary</th>
    <th>tag type</th>
    <th>tag</th>
    <th>information</th>
    <th><span title="tags affecting binary packages are counted once per architecture">count</th>
</tr>
</thead>
<tbody>
  EOF
  res.each do |t|
    if t[:package]
      bin = "<span title=\"#{t[:architectures].join(',')}\">#{esc(t[:package])}/#{esc(t[:package_version])}</span>"
    else
      bin = ""
    end
    puts <<-EOF
    <tr>
      <td class="nowrap"><a href="https://tracker.debian.org/#{esc(t[:source])}">#{esc(t[:source])}</a></td>
      <td class="nowrap">#{esc(t[:version])}</td>
      <td class="nowrap">#{bin}</td>
      <td class="nowrap">#{esc(t[:tag_type])}</td>
      <td class="nowrap">#{esc(t[:tag])}</td>
      <td class="nowrap">#{esc(t[:information])}</td>
      <td class="nowrap">#{t[:count]}</td>
    </tr>
    EOF
  end
  puts <<-EOF
</tbody>
</table>
  EOF
  if res.length == FETCH_LIMIT
    puts "<p>Output limited to the first #{FETCH_LIMIT} results.</p>"
  end
end
puts <<-EOF
</div>
</body>
</html>
EOF

