#!/usr/bin/ruby

$:.unshift('../rlibs')
require 'udd-db'
require 'cgi'
require 'pp'

cgi = CGI::new

if cgi.has_key?('json')
  puts "Content-type: application/json\n\n"
  DB = Sequel.connect(UDD_GUEST)
  q = "select * from sources_patches_status where release='sid' order by source asc"
  r = DB[q].map { |r| r.to_h }
  puts JSON.pretty_generate(r)
  exit(0)
end

puts "Content-type: text/html; charset=utf-8\n\n"

if cgi.has_key?('src')
  src = cgi.params['src'][0]
  if src !~ /^[a-z0-9_.+-]+$/i
    puts "Invalid characters in source"
    exit(0)
  end
else
  src = nil
end

if cgi.has_key?('version')
  version = cgi.params['version'][0]
  if version !~ /^[+~:a-z0-9_.-]+$/i
    puts "Invalid characters in version"
    exit(0)
  end
else
  version = nil
end

puts <<-EOF
<!DOCTYPE html><html><head>
<link href="/css/debian.css" rel="stylesheet" type="text/css">
<link href="/css/udd.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.1.13.6.css"/>
<title>Debian Patches</title>
</head>
<body>
<h1>Debian Patches</h1>
EOF

DB = Sequel.connect(UDD_GUEST)

def show_stats
  puts "<h3>Some statistics</h3>"
  rowsr = DB["select * from releases where role != '' order by sort"].all.sym2str
  releases = []
  rowsr.each do |r|
    next if r['role'] == 'oldoldstable'
    releases << r['release']
  end
  q = <<-EOF
SELECT release,
  COUNT(DISTINCT source) as sources,
  SUM(CASE WHEN format='3.0 (quilt)' THEN 1 ELSE 0 END) as format3_0quilt,
  SUM(CASE WHEN format='3.0 (native)' THEN 1 ELSE 0 END) as format3_0native,
  SUM(CASE WHEN format='1.0' THEN 1 ELSE 0 END) as format1_0,
  SUM(CASE WHEN format='3.0 (quilt)' and patches > 0 THEN 1 ELSE 0 END) as p3patches,
  SUM(patches) as patches,
  SUM(forwarded_invalid) AS forwarded_invalid,
  SUM(forwarded_no) AS forwarded_no,
  SUM(forwarded_not_needed) AS forwarded_not_needed,
  SUM(forwarded_yes) AS forwarded_yes
FROM sources_patches_status
WHERE release IN (#{releases.map { |rel| "'#{rel}'" }.join(',')})
GROUP BY release
  EOF
  res = DB[q].map { |r| r.to_h }
  puts <<-EOF
  <table class="buglist table table-bordered table-hover display compact" id="patches">
<thead>
<tr>
    <th></th>
    #{releases.map { |rel| "<th>#{rel}</th>" }.join}
</tr>
</thead>
<tbody>
  EOF
  puts "<tr><td>Source packages</td>"
  releases.each { |rel| puts "<td>#{res.select { |e| e[:release] == rel }.first[:sources]}</td>" }
  puts "</tr>"

  puts "<tr><td>... packages using format 3.0 (native)</td>"
  releases.each do |rel|
    r = res.select { |e| e[:release] == rel }.first
    t = r[:sources] ; v = r[:format3_0native]
    p = sprintf('%.1f', v*100.0/t)
    puts "<td>#{v} (#{p}%)</td>"
  end
  puts "</tr>"

  puts "<tr><td>... packages using format 1.0</td>"
  releases.each do |rel|
    r = res.select { |e| e[:release] == rel }.first
    t = r[:sources] ; v = r[:format1_0]
    p = sprintf('%.1f', v*100.0/t)
    puts "<td>#{v} (#{p}%)</td>"
  end
  puts "</tr>"

  puts "<tr><td>... packages using format 3.0 (quilt)</td>"
  releases.each do |rel|
    r = res.select { |e| e[:release] == rel }.first
    t = r[:sources] ; v = r[:format3_0quilt]
    p = sprintf('%.1f', v*100.0/t)
    puts "<td>#{v} (#{p}%)</td>"
  end
  puts "</tr>"

  puts "<tr><td>...... 3.0 (quilt) packages with at least one patch</td>"
  releases.each do |rel|
    r = res.select { |e| e[:release] == rel }.first
    t = r[:format3_0quilt] ; v = r[:p3patches]
    p = sprintf('%.1f', v*100.0/t)
    puts "<td>#{v} (#{p}%)</td>"
  end
  puts "</tr>"

  puts "<tr><td>total number of patches (in 3.0 (quilt) packages)</td>"
  releases.each { |rel| puts "<td>#{res.select { |e| e[:release] == rel }.first[:patches].to_i}</td>" }
  puts "</tr>"

  puts "<tr><td>... patches forwarded upstream</td>"
  releases.each do |rel|
    r = res.select { |e| e[:release] == rel }.first
    t = r[:patches] ; v = r[:forwarded_yes]
    p = sprintf('%.1f', v*100.0/t)
    puts "<td>#{v.to_i} (#{p}%)</td>"
  end
  puts "</tr>"

  puts "<tr><td>... patches not yet forwarded upstream</td>"
  releases.each do |rel|
    r = res.select { |e| e[:release] == rel }.first
    t = r[:patches] ; v = r[:forwarded_no]
    p = sprintf('%.1f', v*100.0/t)
    puts "<td>#{v.to_i} (#{p}%)</td>"
  end
  puts "</tr>"

  puts "<tr><td>... patches with no need to forward upstream</td>"
  releases.each do |rel|
    r = res.select { |e| e[:release] == rel }.first
    t = r[:patches] ; v = r[:forwarded_not_needed]
    p = sprintf('%.1f', v*100.0/t)
    puts "<td>#{v.to_i} (#{p}%)</td>"
  end
  puts "</tr>"

  puts "<tr><td>... patches with invalid forwarded value</td>"
  releases.each do |rel|
    r = res.select { |e| e[:release] == rel }.first
    t = r[:patches] ; v = r[:forwarded_invalid]
    p = sprintf('%.1f', v*100.0/t)
    puts "<td>#{v.to_i} (#{p}%)</td>"
  end
  puts "</tr>"

  puts "</tbody></table>"
end

def patches_data(src, version, resp)
  puts <<-EOF
<table class="buglist table table-bordered table-hover display compact" id="patches">
<thead>
<tr>
    <th>Patch</th>
    <th>Description</th>
    <th>Author</th>
    <th>Forwarded</th>
    <th>Bugs</th>
    <th>Origin</th>
    <th>Last update</th>
</tr>
</thead>
<tbody>
  EOF
  resp.each do |e|
    puts "<tr>"
    puts "<td><a href='https://sources.debian.org/src/#{src}/#{CGI.escape(version)}/debian/patches/#{e[:patch]}'>#{e[:patch]}</a></td>"
    puts "<td>#{CGI.escapeHTML((e[:description] || '').strip).gsub("\n", "<br/>")}</pre></td>"
    puts "<td>#{CGI.escapeHTML(e[:author] || '')}</td>"
    if e[:forwarded_short] == 'yes'
      puts "<td><a href='#{e[:forwarded_url]}'>#{CGI.escapeHTML(e[:forwarded_short])}</a></td>"
    elsif e[:forwarded_short] == 'not-needed'
      puts "<td>#{CGI.escapeHTML(e[:forwarded_short])}</td>"
    elsif e[:forwarded_short] == 'invalid'
      puts "<td><span class=\"prio_high\" title=\"#{CGI.escapeHTML(e[:invalid_reason])}\">#{CGI.escapeHTML(e[:forwarded_short])}</span></td>"
    else
      puts "<td><span class=\"prio_high\">#{CGI.escapeHTML(e[:forwarded_short])}</span></td>"
    end
    puts "<td>"
    if e[:debian_bug]
      puts "<a href=\"#{CGI.escapeHTML(e[:debian_bug])}\">debian</a>"
    end
    if e[:upstream_bug]
      puts "<a href=\"#{CGI.escapeHTML(e[:upstream_bug])}\">upstream</a>"
    end
    puts "</td>"
    puts "<td>#{CGI.escapeHTML(e[:origin] || '')}</td>"
    puts "<td>#{e[:last_update] ? e[:last_update].strftime("%Y-%m-%d") : ''}</td>"
    puts "</tr>"
  end
  puts "</tbody></table>"
#  puts "<pre>"
#  pp resp
#  puts "</pre>"
end

def show_versions(src)
  q = "select source, version, release from sources_patches_status where source = '#{src}' order by version desc"
  res = DB[q].map { |r| r.to_h }
  puts "<ul>"
  res.group_by { |e| e[:version] }.to_a.each do |g|
    puts "<li><a href=\"?src=#{src}&version=#{CGI.escape(g[0])}\">#{g[0]}</a> (#{g[1].map { |f| f[:release] }.join(', ')})</li>"
  end
  puts "</ul>"
end

def analyze_params(src, version)
  if src.nil?
    puts <<-EOF
  <form action="?" accept-charset="UTF-8">
  <label for="src">Source:</label>
  <input type="text" name="src" id="src">
  <input type="submit" formnovalidate value="Look up">
  </form>
    EOF
    show_stats()
  else
    if version.nil?
      puts "<h2>Known versions for source package '#{src}'</h2>"
      show_versions(src)
    else
      # TODO add magic to translate version from suites (e.g. &version=unstable)
      puts "<h2>Status for #{src}/#{version}</h2>"
      q = "select distinct format, status, patches from sources_patches_status where source='#{src}' and version='#{version}'"
      ressps = DB[q].map { |r| r.to_h }
      r = ressps.first
      if r.nil?
        puts "This source or version is unknown from this service."
      elsif r[:format] == '3.0 (native)'
        puts "This package uses source format '#{r[:format]}'. That format does not include patches."
      elsif r[:format] == '1.0'
        puts "This package uses source format '#{r[:format]}'. That format might include patches in various ways. This service does not support analyzing packages using this format (and never will). For a discussion of the status of the 1.0 format in Debian, see <a href='https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1007717#384'> this bug</a>."
      elsif r[:format] == '3.0 (quilt)'
        if r[:patches] == 0
          puts "<b>This package does not include any patches.</b>"
        else
          q = "select * from patches where (hash, file) in (select hash, file from sources_patches where source='#{src}' and version='#{version}')"
          resp = DB[q].map { |r| r.to_h }
          patches_data(src, version, resp)
        end
      else
        raise
      end
      puts "<h3>All known versions for source package '#{src}'</h3>"
      show_versions(src)
      puts "<h3>Links</h3>"
      puts "<ul>"
      puts "<li><a href='https://tracker.debian.org/#{src}'>Package Tracker for '#{src}'</a></li>"
      puts "<li><a href='https://udd.debian.org/dmd/?src=#{src}'>Debian Maintainer Dashboard for '#{src}'</a></li>"
      puts "<li><a href='https://sources.debian.org/src/#{src}/#{CGI.escape(version)}'>Browse sources for '#{src}/#{version}' on sources.debian.org</a></li>"
      puts "<li><a href='https://dep-team.pages.debian.net/deps/dep3/'>DEP3: Patch Tagging Guidelines</a></li>"
      puts "</ul>"
    end
  end
end

analyze_params(src, version)
puts <<-EOF
</body>
</html>
EOF

