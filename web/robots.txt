User-agent: *
Disallow: /dmd
Disallow: /dmd.cgi
Allow: /lintian-tag.cgi
Allow: /lintian-tag
Allow: /lintian-tags
Disallow: /lintian
Disallow: /lintian.cgi
Allow: /lintian-tag.cgi
Allow: /lintian-tag
Allow: /lintian-tags
Disallow: /bugs
Disallow: /bugs.cgi
